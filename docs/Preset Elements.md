Preset Elements

INPUTS
<div className="input-box">
	<input type="text" />
</div>

TEXTAREAS
<div className="input-box">
	<textarea></textarea>
</div>

SELECTS
<div className="input-box">
	<select>
		<option></option>
	</select>
</div>

LABELS
<label>First Name</label>

BUTTONS
<button className="btn-theme">Submit</button>
<button className="btn-blue">Submit</button>