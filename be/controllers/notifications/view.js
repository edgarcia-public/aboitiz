const returnData = require("../../helpers/returnData");
const Notification = require("../../models/Notification");

// to view filtered via query params
// if no query params are supplied, will return all
const view = async (req, res) => {
  const q = req.query;

  let filter = {};
  let qRole = q?.role ? { tos: { $in: [q.role] } } : "";
  let qAuthor = q?.author ? { author: q.id } : "";

  if (q) {
    if (q.role) {
      filter = { ...filter, ...qRole, ...qAuthor };
      delete q["role"];
    }
    filter = { ...filter, ...q };
  }

  try {
    const data = await Notification.find(filter, "-__v")
      .sort({ updated_at: -1 })
      .populate({
        path: "author",
        select: "_id first_name last_name middle_name role",
      });

    res.status(200).send(returnData(true, data));
  } catch (error) {
    res.status(500).send(false, { message: "Server error" });
  }
};

module.exports = view;
