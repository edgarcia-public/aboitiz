const Notification = require("../../models/Notification");
const returnData = require("../../helpers/returnData");

const update = async (req, res) => {
  const data = req.body;

  try {

    const updated = await Notification.findOneAndUpdate({ _id: data._id, active: true },
      { ...data, updated_at: Date.now() },
      { upsert: true },
      { new: true }
    );

    if (!updated) return res.status(400).send(returnData(false, "Notification not found."));

    return res.status(200).send(returnData(true, updated));
  } catch (error) {
    res.status(400).send(returnData(false, error));
  }
};

module.exports = update;
