const returnData = require("../../helpers/returnData");
const Notification = require("../../models/Notification");

const add = async (req, res) => {
  const data = req.body;

  delete data._id;

  try {
    const newNotification = new Notification(data);
    const saveNotification = await newNotification.save();

    if (!saveNotification) return res.send(returnData(false, "Failed to post Notification."));

    const findNotification = await Notification.find({ author: data.author, active: true }, "-__v")
      .sort({ updated_at: -1 })
      .populate({ path: "author", select: "_id role first_name middle_name last_name" });

    return res.status(200).send(returnData(true, findNotification))

  } catch (error) {
    console.log(error);
    res.status(500).send({ success: false, message: "Server error" });
  }
};

module.exports = add;
