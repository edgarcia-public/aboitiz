const Notification = require("../../models/Notification");
const returnData = require("../../helpers/returnData");

const viewOne = async (req, res) => {
  const q = req.query;

  try {
    const foundNotification = await Notification.findOne(q,
      "-updated_at"
    ).populate({
      path: "author",
      select: "_id first_name last_name middle_name role",
    });

    if (!foundNotification)
      return res.status(400).send(returnData(false, "Notification not found."));

    return res.status(200).send(returnData(true, foundNotification));
  } catch (error) {
    res.status(400).send(returnData(false, error));
  }
};

module.exports = viewOne;
