const Cfa = require("../../models/cfa");
const Application = require("../../models/Application");
const User = require("../../models/User");
const returnData = require("../../helpers/returnData");

const viewAllDashData = async (req, res) => {
	try {
		const applications = await Application.find({}, "_id status created_at");
		const cfas = await Cfa.find({}, "_id title status created_at");
		const users = await User.find({ status: "pending" }, "_id status created_at");

		const data = {
			applications: applications,
			cfas: cfas,
			users: users ? users : []
		}

		if (data) return res.status(200).send(returnData(true, data));
	} catch (error) {
		res.status(400).send(returnData(false, error));
	}
};

module.exports = viewAllDashData;
