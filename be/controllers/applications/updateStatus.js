const Application = require("../../models/Application");
const returnData = require("../../helpers/returnData");

const updateStatus = async (req, res) => {
	const formData = { ...req.body, updated_at: Date.now() };

	try {
		const application = await Application.findOneAndUpdate({ _id: formData._id }, { $set: { ...formData, "form_details.status": formData.status } }, { new: true })
		if (!application) return res.status(400).send(returnData(false, "Failed saving profile. Please refresh the page."));

		const pending = await Application.find({ status: "pending" })
			.populate({ path: "cfa_id" })
		const review = await Application.find({ status: "review" })
			.populate({ path: "cfa_id" })
		const shortlist = await Application.find({ status: "shortlist" })
			.populate({ path: "cfa_id" })
		const approve = await Application.find({ status: "approve" })
			.populate({ path: "cfa_id" })
		const reject = await Application.find({ status: "reject" })
			.populate({ path: "cfa_id" })

		return res.status(200).send(returnData(true, application, {
			pending: pending,
			review: review,
			shortlist: shortlist,
			approve: approve,
			reject: reject
		}));
	} catch (error) {
		return res.status(500).send(returnData(false, error));
	}
}

module.exports = updateStatus;
