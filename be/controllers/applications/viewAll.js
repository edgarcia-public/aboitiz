const Application = require("../../models/Application");
const returnData = require("../../helpers/returnData");

const viewAll = async (req, res) => {
  const q = req.query;

  try {
    const data = await Application.find(q)
      .populate({ path: "cfa_id", select: "title" })
      .populate({
        path: "user_id",
        select: "_id first_name middle_name last_name middle_name role status",
      })

    if (data) return res.status(200).send(returnData(true, data));
  } catch (error) {
    res.status(400).send(returnData(false, error));
  }
};

module.exports = viewAll;
