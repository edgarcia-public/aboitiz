const Application = require("../../models/Application");
const CFA = require("../../models/cfa");
const returnData = require("../../helpers/returnData");

const add = async (req, res) => {
  const formData = { ...req.body };

  if (!formData.user_id) return res.status(401).send(returnData(true, "You need an active account to submit the application form."));
  if (!formData.cfa_id) return res.status(401).send(returnData(true, "The application form failed to submit. Please refresh the page and try again."));

  try {
    const onReview = await Application.findOne({
      user_id: formData.user_id,
      status: "review",
    });

    // user has an on review application
    if (onReview) return res.status(400).send(returnData(false, "Sorry, you still have a pending application currently on review."));

    const updatedForm = await Application.findOneAndUpdate({
      user_id: formData.user_id,
      status: "pending",
    }, { $set: { "form_details": formData, updated_at: Date.now() } }, { new: true });

    if (updatedForm) return res.status(200).send(returnData(true, updatedForm));

    // no current form    
    if (!updatedForm) {
      let externalLink;


      if (formData.external) {
        const cfa = await CFA.findOne({ _id: formData.cfa_id }, "title form external canvas status created_at");

        externalLink = cfa.external;
      }

      const data = {
        status: "pending",
        user_id: formData.user_id,
        cfa_id: formData.cfa_id,
        form_title: formData.form_title,
        external: externalLink,
        form_details: { ...formData, external: externalLink }
      }

      const addForm = new Application(data);
      const saveForm = await addForm.save();

      if (saveForm) return res.status(200).send(returnData(true, addForm));
    }
  } catch (error) {
    return res.status(400).send(returnData(false, "Something went wrong. Please refresh the page and try again."));
  }
};

module.exports = add;
