const Application = require("../../models/Application");
const returnData = require("../../helpers/returnData");

const viewUserForm = async (req, res) => {
	try {
		const findUserForm = await Application.findOne({ ...req.query })
			.populate({
				path: "cfa_id",
				select: "-canvas"
			})
			.populate({
				path: "user_id",
				select: "-password -status",
			});

		if (!findUserForm) return res.status(400).send(returnData(false, "Application Form not found."));

		return res.status(200).send(returnData(true, { ...findUserForm.form_details }));
	} catch (error) {
		res.status(400).send(returnData(false, error));
	}
};

module.exports = viewUserForm;
