const Application = require("../../models/Application");
const CFA = require("../../models/cfa");
const returnData = require("../../helpers/returnData");

const viewOne = async (req, res) => {
  const q = req.query;

  try {
    const findCfa = await CFA.findOne({ _id: q.cfa_id });

    if (!findCfa) return res.status(400).send(returnData(false, "CFA not found."));

    const data = await Application.find(q)
      .populate({
        path: "cfa_id",
        select: "-canvas"
      })
      .populate({
        path: "user_id",
        select: "-password -status",
      });

    return res.status(200).send(returnData(true, ...data));
  } catch (error) {
    res.status(400).send(returnData(false, error));
  }
};

module.exports = viewOne;
