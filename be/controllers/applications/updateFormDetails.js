const Application = require("../../models/Application");
const returnData = require("../../helpers/returnData");

const updateFormDetails = async (req, res) => {
	const formDetailsData = { ...req.body };

	try {
		const application = await Application.findOneAndUpdate({ _id: formDetailsData._id }, { $set: { updated_at: Date.now(), "form_details": formDetailsData } }, { new: true })

		if (!application) return res.status(400).send(returnData(false, "Failed saving profile. Please refresh the page."));

		return res.status(200).send(returnData(true, application));
	} catch (error) {
		return res.status(500).send(returnData(false, error));
	}
}

module.exports = updateFormDetails;
