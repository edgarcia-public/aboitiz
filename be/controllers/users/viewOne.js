const User = require("../../models/User");
const returnData = require("../../helpers/returnData");

const viewOne = async (req, res) => {
  const userId = req.params.user_id;

  const user = await User.findOne({ _id: userId });
  if (!user) return returnData(false, user);

  // return just the needed data
  const data = {
    ...user._doc,
  };

  delete data.password;
  delete data.__v;
  delete data.updatedAt;

  return res.status(200).send(returnData(true, data));
};

module.exports = viewOne;
