const User = require("../../models/User");
const bcrypt = require("bcrypt");
const returnData = require("../../helpers/returnData");

const add = async (req, res) => {
	const userData = { ...req.body }

	// remove retry password
	delete userData.retry;

	// checks for duplicate email
	const emailExist = await User.findOne({ email: userData.email }, "email");

	// found a duplicate
	if (emailExist) return res.status(400).send(returnData(true, "Email address exists. Please log in instead."));

	// get all users
	const getAllUsers = await User.find({});

	// check if the new user is an admin or officer
	if (userData.new_admin) {
		const addNewAdmin = new User({ ...userData });
		const saveAdmin = await addNewAdmin.save();

		if (!saveAdmin) return res.status(400).send(returnData(false, "Sign up failed."));

		return res.status(200).send(returnData(true, `An ${userData.role} user added successfully.`));
	} else {
		const superUser = getAllUsers.length ? undefined : { role: "super", status: "active" };

		// create a hash password first!
		const salt = await bcrypt.genSalt(3);
		const pwHash = await bcrypt.hash(userData.password, salt);

		const addUser = new User({ ...userData, password: pwHash, ...superUser }, "_id role status");
		const saveUser = await addUser.save();

		// failed to save
		if (!saveUser) return res.status(400).send(returnData(false, "Sign up failed."));

		// select field
		const user = await User.findById(addUser._id).select("_id role status");

		return res.status(200).send(returnData(true, user));
	}
}

module.exports = add;
