const User = require("../../models/User");
const bcrypt = require("bcrypt");
const returnData = require("../../helpers/returnData");

const resetPassword = async (req, res) => {
	// pass -> email, currentPassword, and newPassword
	const userData = { ...req.body }

	// remove retry password
	delete userData.retry;

	// double check email
	const userFound = await User.findOne({ email: userData.email }, "_id email password new_admin");

	// email NOT found
	if (!userFound) return res.status(400).send(returnData(true, "Email address not found. Please Sign Up instead."));

	// check if new admin
	if (userFound.new_admin) {
		if (userFound.password !== userData.currentPassword) return res.status(400).send(returnData(false, "The Default Password is incorrect. Please retry."));

		const response = await doResetPassword(userFound.email, { password: userData.newPassword, new_admin: false });

		if (response.success) {
			return res.status(200).send(returnData(response.success, response.data));
		} else {
			return res.status(400).send(returnData(response.success, response.data));
		}
	} else {
		const passwordCheck = await bcrypt.compare(userData.currentPassword, userFound.password);

		if (!passwordCheck) return res.status(400).send(returnData(false, "The Current Password is incorrect. Please retry."));

		const response = await doResetPassword(userFound.email, { password: userData.newPassword });

		if (response.success) {
			return res.status(200).send(returnData(response.success, response.data));
		} else {
			return res.status(400).send(returnData(response.success, response.data));
		}
	}
}

async function doResetPassword(email, newData) {
	// create a hash password first!
	const salt = await bcrypt.genSalt(3);
	const pwHash = await bcrypt.hash(newData.password, salt);

	try {
		const response = await User.findOneAndUpdate(
			{ email: email },
			{ ...newData, password: pwHash, updated_at: Date.now() },
			{ new: true }
		);

		if (response) {
			return {
				success: true,
				data: {
					_id: response._id,
					role: response.role,
					status: response.status,
					new_admin: response.new_admin
				}
			}
		} else {
			return {
				success: false,
				data: "Something went wrong. Please refresh the page and try again."
			}
		}
	} catch (error) {
		return {
			success: false,
			data: error
		}
	}
}

module.exports = resetPassword;