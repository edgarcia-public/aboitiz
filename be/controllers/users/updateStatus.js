const User = require("../../models/User");
const returnData = require("../../helpers/returnData");

const updateStatus = async (req, res) => {
	const userData = { ...req.body, updated_at: Date.now() };
	const q = { ...req.query }

	try {
		const user = await User.findOneAndUpdate({ _id: userData._id }, userData, { new: true })
		if (!user) return res.status(400).send(returnData(false, "Failed saving profile. Please refresh the page."));

		const data = await User.find(q, "_id first_name middle_name last_name email role created_at status");

		return res.status(200).send(returnData(true, data));
	} catch (error) {
		return res.status(500).send(returnData(false, error));
	}
}

module.exports = updateStatus;
