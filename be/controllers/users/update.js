const User = require("../../models/User");
const returnData = require("../../helpers/returnData");

const update = async (req, res) => {
	const userData = { ...req.body, updated_at: Date.now() };

	const user = await User.findOneAndUpdate({ _id: userData._id }, userData, { new: true })
	if (!user) return res.status(400).send(returnData(false, "Failed saving profile. Please refresh the page."));

	const data = {
		...user._doc
	}

	// return just the needed data
	delete data.password;
	delete data.__v;
	delete data.updatedAt;

	return res.status(200).send(returnData(true, data));
}

module.exports = update;
