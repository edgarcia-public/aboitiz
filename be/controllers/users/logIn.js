const User = require("../../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const returnData = require("../../helpers/returnData");

const logIn = async (req, res) => {
	const userData = { ...req.body }

	// checks for duplicate email
	const user = await User.findOne({ email: userData.email }, "_id password role status new_admin")

	if (!user) return res.status(400).send(returnData(false, "The email address doesn't exist. Please retry or Sign Up!"));

	// checks if admin or officer
	if (user.new_admin) {
		if (user.password !== userData.password) return res.status(400).send(returnData(false, "The Default Password is incorrect."));

		return res.status(200).send(returnData(true, user, "new_admin"));
	} else {

		const passwordCheck = await bcrypt.compare(userData.password, user.password);

		// check password if matched
		if (!passwordCheck) return res.status(400).send(returnData(false, "The email and/ or password is incorrect"));

		// remove password
		const data = {
			...user._doc
		}
		delete data.password;

		return res.status(200).send(returnData(true, data));
	}
}

module.exports = logIn;
