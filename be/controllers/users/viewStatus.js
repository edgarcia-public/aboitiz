const returnData = require("../../helpers/returnData");
const User = require("../../models/User");

const viewStatus = async (req, res) => {
  const q = req.query;

  try {
    const data = await User.find(q, "_id first_name middle_name last_name email role created_at status");

    return res.status(200).send(returnData(true, data));
  } catch (error) {
    return res.status(500).send(false, { message: error });
  }
};

module.exports = viewStatus;
