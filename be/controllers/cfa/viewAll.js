const returnData = require("../../helpers/returnData");
const Cfa = require("../../models/cfa");

const viewAll = async (req, res) => {
  try {
    const data = await Cfa.find(
      {},
      "_id title status created_at author form"
    ).populate("author", "first_name last_name middle_name role");

    res.status(200).send(returnData(true, data));
  } catch (error) {
    res.status(500).send(false, { message: "Server error" });
  }
};

module.exports = viewAll;
