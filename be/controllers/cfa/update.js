const CFA = require("../../models/cfa");
const returnData = require("../../helpers/returnData");

const update = async (req, res) => {
	const cfaData = { ...req.body, updated_at: Date.now() };

	delete cfaData.created_at;

	const updatedCfa = await CFA.findOneAndUpdate({ _id: cfaData._id }, cfaData, { new: true })

	if (!updatedCfa) return res.status(400).send(returnData(false, "Failed updating CFA. Please refresh the page."));

	return res.status(200).send(returnData(true, updatedCfa));
}

module.exports = update;
