const CFA = require("../../models/cfa");
const returnData = require("../../helpers/returnData");

const viewOne = async (req, res) => {
  try {
    const cfaId = req.params.cfa_id;

    const cfa = await CFA.findOne(
      { _id: cfaId },
      "title form external canvas status created_at"
    );
    if (!cfa) return res.status(404).send(returnData(false, "CFA not found."));

    return res.status(200).send(returnData(true, cfa));
  } catch (error) {
    return res.status(500).send(returnData(false, "Internal server error."));
  }
};

module.exports = viewOne;
