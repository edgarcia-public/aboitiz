const CFA = require("../../models/cfa");
const returnData = require("../../helpers/returnData");

const add = async (req, res) => {
	const cfaData = {
		...req.body
	}

	try {
		const addCfa = new CFA(cfaData);
		const saveCfa = await addCfa.save();

		if (!saveCfa) return res.status(400).send(returnData(false, "Failed to save CFA."));

		const newCfaData = {
			...saveCfa._doc,
			canvas: cfaData.form === "external" ? addCfa.canvas : changeHrefValues(addCfa.canvas, `/applications/forms/${addCfa.form}/${saveCfa._id}`)
		}
		const updatedCfa = await CFA.findOneAndUpdate({ _id: saveCfa._id }, newCfaData, { new: true })

		return res.status(200).send(returnData(true, updatedCfa));
	} catch (error) {
		return res.status(400).send(returnData(false, "Failed to save CFA."));
	}
}

function changeHrefValues(inputString, newHref) {
	var regex = /<a[^>]*href="([^"]*)"[^>]*>/g;
	var replacedString = inputString.replaceAll(regex, function (match, hrefValue) {
		var newTag = match.replaceAll(hrefValue, newHref);

		return newTag;
	});

	return replacedString;
}

module.exports = add;
