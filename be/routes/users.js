const express = require("express");
const router = express.Router();

const add = require("../controllers/users/add");
const logIn = require("../controllers/users/logIn");
const viewOne = require("../controllers/users/viewOne");
const update = require("../controllers/users/update");
const viewStatus = require("../controllers/users/viewStatus");
const resetPassword = require("../controllers/users/resetPassword");
const updateStatus = require("../controllers/users/updateStatus");

router.post("/add", add);
router.post("/log-in", logIn);
router.patch("/update", update);
router.patch("/update-status", updateStatus);
router.patch("/reset-password", resetPassword);

// by mon
router.get("/view", viewStatus);
router.get("/view/:user_id", viewOne);

module.exports = router;
