const express = require("express");
const router = express.Router();

const viewAllDashData = require("../controllers/dashboard/viewAllDashData");

router.get("/view-all-dash-data", viewAllDashData);

module.exports = router;
