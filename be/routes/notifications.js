const express = require("express");
const router = express.Router();

const add = require("../controllers/notifications/add");
const view = require("../controllers/notifications/view");
const viewOne = require("../controllers/notifications/viewOne");
const update = require("../controllers/notifications/update");

router.post("/add", add);
router.patch("/update", update);
router.get("/viewOne", viewOne);
router.get("/view/:q?", view);

module.exports = router;
