const express = require("express");
const router = express.Router();

const viewAll = require("../controllers/applications/viewAll");
const add = require("../controllers/applications/add");
const viewOne = require("../controllers/applications/viewOne");
const viewUserForm = require("../controllers/applications/viewUserForm");
const updateStatus = require("../controllers/applications/updateStatus");
const updateFormDetails = require("../controllers/applications/updateFormDetails");

router.post("/add", add);
router.get("/view-all", viewAll);
router.get("/view", viewOne);
router.get("/view-user-form", viewUserForm);
router.patch("/update-status", updateStatus);
router.patch("/update-form-details", updateFormDetails);

module.exports = router;
