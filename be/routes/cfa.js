const express = require("express");
const router = express.Router();

const add = require("../controllers/cfa/add");
const viewOne = require("../controllers/cfa/viewOne");
const update = require("../controllers/cfa/update");
const viewAll = require("../controllers/cfa/viewAll");

router.post("/add", add);
router.get("/view/:cfa_id", viewOne);
router.get("/view/", viewAll);
router.put("/update", update);

module.exports = router;
