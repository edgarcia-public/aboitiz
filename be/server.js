const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const mongoose = require("mongoose");
require("dotenv").config();

const PORT = process.env.PORT;
const DB_URI = process.env.ATLAS_URL;

const app = express();

// Connect to MongoDB
mongoose
  .connect(DB_URI)
  .then(() => {
    console.log("DB Connected");
  })
  .catch((error) => {
    console.log(error);
  });

app.use(cors());
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const users = require("./routes/users");
const cfa = require("./routes/cfa");
const applications = require("./routes/applications");
const dashboard = require("./routes/dashboard");
const announcements = require("./routes/notifications");
const notifications = require("./routes/notifications");

app.use("/users", users);
app.use("/cfa", cfa);
app.use("/applications", applications);
app.use("/dashboard", dashboard);
app.use("/announcements", announcements);
app.use("/notifications", notifications);

// Start server
app.listen(PORT, () => console.log(`Listens to ${PORT}`));
