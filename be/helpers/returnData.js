/*
returnData helper
params:
1. status:Boolean = [true or false]
2. data:* = from mongoDB or error message
*/

const returnData = (success = true, rawData = "", rawExtra = "") => {
	const stringData = JSON.stringify(rawData);
	const stringExtra = JSON.stringify(rawExtra);

	let obj = {
		success: success,
		data: JSON.parse(stringData),
		extra: JSON.parse(stringExtra)
	}

	return obj;
}

module.exports = returnData;