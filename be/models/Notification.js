const mongoose = require("mongoose");

const notificationSchema = new mongoose.Schema({
  tos: [{ type: String }],
  message: String,
  active: { type: Boolean, default: true },
  author: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Notification", notificationSchema);
