const mongoose = require("mongoose");

const User = new mongoose.Schema({
	age: Number,
	gender: String,
	email: String,
	password: String,
	first_name: String,
	middle_name: String,
	last_name: String,
	role: String,
	status: String,
	address: String,
	region: String,
	province: String,
	city: String,
	town: String,
	new_admin: Boolean,
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model("User", User);
