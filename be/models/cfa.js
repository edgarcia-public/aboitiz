const mongoose = require("mongoose");

const CFA = new mongoose.Schema({
	title: String,
	form: String,
	external: String,
	canvas: String,
	status: String,
	author: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
	created_at: { type: Date, default: Date.now },
	updated_at: { type: Date, default: Date.now }
})

module.exports = mongoose.model("CFA", CFA);
