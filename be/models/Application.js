const mongoose = require("mongoose");

const applicationsSchema = new mongoose.Schema({
  status: { type: String, default: "pending" },
  user_id: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  cfa_id: { type: mongoose.Schema.Types.ObjectId, ref: "CFA" },
  form_title: String,
  external: String,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  form_details: {
    type: Object
  },
});

module.exports = mongoose.model("Application", applicationsSchema);
