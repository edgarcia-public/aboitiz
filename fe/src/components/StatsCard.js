import "../assets/css/statsCard.css";
import { ImNewspaper } from "react-icons/im";
import { BsAward, BsPersonFillAdd, BsCheckSquare, BsPatchQuestion, BsSend, BsXOctagon, } from "react-icons/bs";
import { useContext, useEffect, useState } from "react";
import { viewAllDashData } from "../helpers/endpoints/dashboard";
import getCurrentPhDate from "../helpers/getCurrentPhDate";
import removeChar from "../helpers/removeChar";
import { GlobalContext } from "../App";

const StatsCard = () => {
  const { setNotif } = useContext(GlobalContext);
  const [allDashData, setAllDashData] = useState("");
  const [cardData, setCardData] = useState({
    call_for_applications: {
      icon: <ImNewspaper />,
      count: 0
    },
    total_applications: {
      icon: <BsSend />,
      count: 0
    },
    pending_applications: {
      icon: <BsPatchQuestion />,
      count: 0
    },
    shortlisted_applications: {
      icon: <BsCheckSquare />,
      count: 0
    },
    approved_applications: {
      icon: <BsAward />,
      count: 0
    },
    rejected_applications: {
      icon: <BsXOctagon />,
      count: 0
    },
    new_sign_ups: {
      icon: <BsPersonFillAdd />,
      count: 0
    }
  })

  useEffect(() => {
    const getAllDashData = async () => {
      if (!allDashData) {

        await viewAllDashData()
          .then(response => {
            setNotif("Displaying Dashboard data...");

            setAllDashData(response);

            setCardData({
              ...cardData,
              total_applications: { ...cardData.total_applications, count: response.data.applications.length },
              pending_applications: { ...cardData.pending_applications, count: response.data.applications.filter(item => item.status === "pending").length },
              shortlisted_applications: { ...cardData.shortlisted_applications, count: response.data.applications.filter(item => item.status === "shortlist").length },
              approved_applications: { ...cardData.approved_applications, count: response.data.applications.filter(item => item.status === "approve").length },
              rejected_applications: { ...cardData.rejected_applications, count: response.data.applications.filter(item => item.status === "reject").length },
              call_for_applications: { ...cardData.call_for_applications, count: response.data.cfas.length },
              new_sign_ups: { ...cardData.new_sign_ups, count: response.data.users.length },
            })
          })
          .catch(error => {
            // const response = error.response.data;
            console.log(error);
          })
      }
    }
    getAllDashData();
  }, [allDashData, cardData, setNotif])

  return (
    <section className="StatsCard">
      <div className="container">
        <div className="StatsCard-list">
          <div className="panel">
            <div className="panel-header bg-blue">
              <p className="StatsCard-title nogaps">Date</p>
            </div>
            <div className="panel-body">
              <div className="StatsCard-body">
                <div className="StatsCard-content StatsCard-value">{getCurrentPhDate()}</div>
              </div>
            </div>
          </div>
          {Object.keys(cardData).map((key, index) =>
            <div key={index} className="panel">
              <div className="panel-header bg-blue">
                <p className="StatsCard-title nogaps">{removeChar(key, "_")}</p>
              </div>
              <div className="panel-body">
                <div className="StatsCard-body">
                  <div className="StatsCard-content StatsCard-icon">{cardData[key].icon}</div>
                  <div className="StatsCard-content StatsCard-value">{cardData[key].count}</div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default StatsCard;
