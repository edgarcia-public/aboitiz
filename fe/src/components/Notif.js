import { useContext, useEffect } from "react";
import { GlobalContext } from "../App";

const Notif = ({ message }) => {
	const { setNotif } = useContext(GlobalContext);

	useEffect(() => {
		const notifElem = document.getElementById("Notif");

		if (notifElem) {
			setTimeout(() => {
				setNotif("");
			}, 2000);
		}
	}, [setNotif])

	return (
		<div id="Notif">
			<div className="Notif--icon-box">
				<img src={require("../assets/images/utilities/notif.png")} width="20" alt="" />
			</div>
			<div className="Notif--message-box">
				<p>{message ? message : "Something went wrong. Please refresh the page and try again."}</p>
			</div>
		</div>
	)
}

export default Notif;