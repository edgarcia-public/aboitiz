const TableTr = ({ tr }) => {
  const trData = {
    ...tr
  }

  return (
    <tr>
      {Object.keys(trData).map((field, index) => (
        <td key={index} className="table-tr">
          <div className="panel-body">
            {trData[field]}
          </div>
        </td>
      ))}
    </tr>
  );
};

export default TableTr;
