import "../../assets/css/htmltable.css";
import sortIcon from "../../assets/images/utilities/sort.png";
import { useEffect, useState } from "react";
import TableTr from "./TableTr";
import removeChar from "../../helpers/removeChar";
import Loading from "../../components/Loading";

const HTMLTable = ({ tableData }) => {
  const [sortConfig, setSortConfig] = useState({ key: null, direction: "" });
  const [data, setData] = useState([]);

  useEffect(() => {
    if (!tableData.length) return null;
    setData(tableData);
  }, [tableData, tableData.length])

  const sortTable = (key) => {
    let direction = "asc";
    if (sortConfig.key === key && sortConfig.direction === "asc") {
      direction = "desc";
    }
    setData([...data].sort(compareValues(key, direction)));
    setSortConfig({ key, direction });
  };

  const compareValues = (key, direction) => {
    return (a, b) => {
      const valueA = a[key].toString().toLowerCase();
      const valueB = b[key].toString().toLowerCase();

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
      } else if (valueA < valueB) {
        comparison = -1;
      }

      return direction === "desc" ? comparison * -1 : comparison;
    };
  };

  if (!data.length) return <Loading />;

  return (
    <section className="HTMLTable">
      <table>
        <thead>
          <tr>
            {data ?
              Object.keys(data[0]).map((label, index) =>
                <th key={index}>
                  <div className="panel-header bg-blue">
                    {removeChar(label, "_-") === "actions" ?
                      removeChar(label, "_-")
                      :
                      <>
                        {removeChar(label, "_-")} <img src={sortIcon} onClick={() => sortTable(label)} alt="" title="Click to sort." className="sort-icon" />
                      </>
                    }
                  </div>
                </th>)
              :
              undefined
            }
          </tr>
        </thead>
        <tbody className="panel">
          {data ? data.map((tr, index) => <TableTr key={index} tr={tr} />) : undefined}
        </tbody>
      </table>
    </section>
  );
};

export default HTMLTable;
