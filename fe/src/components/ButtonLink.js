import React from "react";
import { NavLink } from "react-router-dom";

const ButtonLink = ({ label, to }) => {
  // test
  return (
    <NavLink to={to}>
      <button className="btn-blue">{label}</button>
    </NavLink>
  );
};

export default ButtonLink;
