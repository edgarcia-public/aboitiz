import loading from "../assets/images/utilities/loading.gif";

const Loading = () => {
	return (
		<div className="container text-center">
			<img src={loading} className="loading" alt="" />
		</div>
	)
}

export default Loading; 