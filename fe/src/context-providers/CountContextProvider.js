import { createContext, useContext, useReducer } from "react";
import reducer from "./reducer";

const initialState = {
  registrationCount: 0,
  applicationsCount: 0,
  pendingCount: 0,
  shortListCount: 0,
  approvedCount: 0,
  rejectCount: 0,
  cfaCount: 0,
};

const Context = createContext(initialState);

export const useValue = () => {
  return useContext(Context);
};

const ContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <Context.Provider value={{ state, dispatch }}>{children}</Context.Provider>
  );
};

export default ContextProvider;
