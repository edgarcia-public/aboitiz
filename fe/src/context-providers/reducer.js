const reducer = (state, action) => {
  switch (action.type) {
    case "UPDATE_REGISTRATION_COUNT":
      return { ...state, registrationCount: action.payload };
    case "UPDATE_CFA_COUNT":
      return { ...state, cfaCount: action.payload };
    case "UPDATE_APPLICATION_COUNT":
      return { ...state, applicationsCount: action.payload };
    case "UPDATE_PENDING_COUNT":
      return { ...state, pendingCount: action.payload };
    case "UPDATE_SHORTLIST_COUNT":
      return { ...state, shortListCount: action.payload };
    case "UPDATE_REJECT_COUNT":
      return { ...state, rejectCount: action.payload };
    case "UPDATE_APPROVED_COUNT":
      return { ...state, approvedCount: action.payload };
    default:
      throw new Error("No matched action");
  }
};

export default reducer;
