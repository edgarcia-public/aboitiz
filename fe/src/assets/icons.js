import { ImSwitch, ImDisplay, ImFilesEmpty, ImNewspaper } from "react-icons/im";
import { BsAward, BsBell, BsPersonVcard } from "react-icons/bs";

const linkIcons = {
  dashboard: <ImDisplay />,
  scholarships: <BsAward />,
  registrations: <ImFilesEmpty />,
  profile: <BsPersonVcard />,
  none: <ImSwitch />,
  applications: <ImNewspaper />,
  notifications: <BsBell />,
};

export default linkIcons;
