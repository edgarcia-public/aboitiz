import { createContext, useEffect, useState } from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { checkSession } from "./helpers/checkSession";
import MainLayout from "./layouts/MainLayout";
import NotFound from "./pages/error/NotFound";
import Home from "./pages/home/Home";
import UsersController from "./pages/page-controller/UsersController";
import DashboardController from "./pages/page-controller/DashboardController";
import Notif from "./components/Notif";
import CFAController from "./pages/page-controller/CFAController";
import FormController from "./pages/page-controller/FormController";
import CFALanding from "./pages/cfa/CFALanding";
import ContextProvider from "./context-providers/CountContextProvider";
import Templates from "./pages/templates/Templates";

export const GlobalContext = createContext();

const App = () => {
  const [session, setSession] = useState("");
  const [notif, setNotif] = useState("");

  useEffect(() => {
    if (!session && checkSession()) return setSession(checkSession());
  }, [session]);

  // Router config
  const router = createBrowserRouter([
    {
      path: "/",
      element: <MainLayout />,
      children: [
        { path: "", element: <Home /> },
        {
          path: "users/:page",
          element: <UsersController />,
        },
        {
          path: "dashboard/call-for-application/templates",
          element: <Templates />,
        },
        {
          path: "dashboard/call-for-application/:crud/:template_id",
          element: <CFAController />,
        },
        {
          path: "dashboard/:page?/:crud?",
          element: <DashboardController />,
        },
        {
          path: "landing/cfa/:cfa_id",
          element: <CFALanding />,
        },
        {
          path: "applications/forms/:form_title/:cfa_id/:user_id?",
          element: <FormController />,
        },
        {
          path: "*",
          element: <NotFound />,
        },
      ],
    },
  ]);

  document.title = "Home";
  return (
    <div className="App">
      <GlobalContext.Provider value={{ session: session, setSession: setSession, setNotif: setNotif }}>
        <ContextProvider>
          <RouterProvider router={router} />
          {notif && <Notif message={notif} />}
        </ContextProvider>
      </GlobalContext.Provider>
    </div>
  );
};

export default App;
