import React, { useContext, useEffect, useState } from "react";
import { add, viewOne, patch } from "../../helpers/endpoints/notifications";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";

const roles = ["super", "admin", "officer", "applicant", "scholar"];

const Form = ({ setNotifications, editId, setEditId }) => {
  const session = checkSession();
  const { setNotif } = useContext(GlobalContext);
  const [data, setData] = useState({
    _id: "",
    tos: [],
    message: "",
    author: session.data._id,
    status: "active"
  });

  useEffect(() => {
    const getNotifData = async () => {
      await viewOne({ "_id": editId, "author": session.data._id })
        .then(response => {
          console.log(response)

          setData({
            ...data,
            _id: response.data._id,
            tos: response.data.tos,
            message: response.data.message,
            status: "active"
          });

          setEditId("");
        })
        .catch(error => {
          const response = error.response.data;

          setNotif(response.data);
        })
    }

    if (editId) getNotifData();
  }, [data, editId, session.data._id, setEditId, setNotif])

  const handleChange = (e) => {
    if (e.target.type === "checkbox") {

      // value already in the array
      if (data.tos.includes(e.target.value)) {

        setData({
          ...data,
          tos: [...data.tos.filter(to => to !== e.target.value)]
        });

      } else {
        setData({ ...data, tos: [...data.tos, e.target.value] })
      }
    } else {
      setData({ ...data, [e.target.name]: e.target.value })
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!data.tos.length || !data.message) return setNotif("All fields are required.");

    const saveNotif = async () => {
      await add(data)
        .then(response => {
          setNotifications(response.data);
          setData({ ...data, message: "", tos: [] })
          setNotif("Notification added successfully. Updating data...");
        })
        .catch(error => {
          const response = error.response.data;

          setNotif(response.data);
        })
    }

    saveNotif();
  };

  const handleUpdate = (e) => {
    e.preventDefault();

    if (!data.tos.length || !data.message) return setNotif("All fields are required.");

    const udpateNotif = async () => {
      await patch(data)
        .then(response => {
          setNotifications(response.data);
          setData({ ...data, _id: "", tos: [], message: "" });
          setEditId("");
          setNotif("Notification updated successfully. Updating data...");
        })
        .catch(error => {
          const response = error.response.data;

          setNotif(response.data);
        })
    }

    udpateNotif();
  }

  console.log(data.tos);

  return (
    <div>
      <form className="Notifications-form" onSubmit={data._id ? handleUpdate : handleSubmit}>
        <div className="Notifications-form-to">
          <small className="elem-block margin-bottom-10">Send to</small>
          <div className="roles">
            {roles.map((role, index) =>
              <label key={index}>
                <input
                  type="checkbox"
                  value={role}
                  onChange={handleChange}
                  checked={data.tos.includes(role)}
                />
                {role}
              </label>)}
          </div>
        </div>
        <div className="Notifications-form-message">
          <small className="elem-block margin-bottom-10">Message</small>
          <div className="input-box margin-bottom-5">
            <textarea
              rows={4}
              cols={40}
              name="message"
              value={data.message || ""}
              onChange={handleChange}
            />
          </div>
          <button type="submit" className="btn btn-green">POST</button>
        </div>
      </form>
    </div>
  );
};

export default Form;
