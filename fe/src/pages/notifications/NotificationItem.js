import capitalize from "../../helpers/capitalize";
import formatDatefromUTC from "../../helpers/formatDatefromUTC";
import DOMpurify from "dompurify";
import { patch } from "../../helpers/endpoints/notifications";
import { GlobalContext } from "../../App";
import { useContext } from "react";
import "../../assets/css/notifications.css";

const NotificationItem = ({ data, setNotifications, session, page, setEditId }) => {
	const { setNotif } = useContext(GlobalContext);
	const handleOnNotifEdit = (e) => {
		setEditId(e.target.id);
	}

	const handelDelete = (e) => {
		e.preventDefault();

		const udpateNotif = async () => {
			await patch({ _id: e.target.id, active: false })
				.then(response => {
					setNotifications(response.data);
					setNotif("Notification updated successfully. Updating data...");
				})
				.catch(error => {
					const response = error.response.data;

					setNotif(response.data);
				})
		}

		udpateNotif();
	}

	return (
		<div className="panel Notifications-entry">

			<div className="panel-header">
				<label>TO: </label>
				<div>
					{data.tos.map((to, index) => <span key={index} className="capsule">{capitalize(to)}</span>)}
				</div>
			</div>
			<div className="panel-body">
				<small className="elem-block margin-bottom-15 color-gray">{formatDatefromUTC(data.created_at)}</small>
				{<span dangerouslySetInnerHTML={{ __html: DOMpurify.sanitize(data.message.toString()) }} />}
			</div>
			<div className="panel-footer">
				<div>
					<label>FROM: </label> <span className="capsule bordered">
						{data.author.first_name} {data.author.middle_name} {data.author.last_name} <small>({capitalize(data.author.role)})</small>
					</span>
				</div>
				{session.data._id === data.author._id && (page === "notifications" && data.active) ?
					<div>
						<button className="btn btn-sm" id={data._id} onClick={handleOnNotifEdit}>Edit</button> <button className="btn btn-sm btn-danger" id={data._id} onClick={handelDelete}>Delete</button>
					</div>
					:
					!data.active ? <small className="color-theme">REMOVED</small> : undefined
				}
			</div>
		</div>
	);
}

export default NotificationItem;