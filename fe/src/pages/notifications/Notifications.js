import { useEffect, useState } from "react";
import "../../assets/css/notifications.css";
import Form from "./Form";
import NotificationItem from "./NotificationItem";
import { view } from "../../helpers/endpoints/notifications";
import { checkSession } from "../../helpers/checkSession";

const Notifications = ({ page }) => {
  const session = checkSession();
  const [notifications, setNotifications] = useState([]);
  const [editId, setEditId] = useState("");

  useEffect(() => {
    if (!notifications.length) {

      if (page === "notifications") {
        let q;

        switch (session.data.role) {
          case "super":
          case "admin":
          case "officer":
            q = { author: session.data._id, active: true }
            break;
          case "applicant":
            q = { tos: "applicant", active: true }
            break;
          case "scholar":
            q = { tos: "scholar", active: true }
            break;
          default:
            break;
        }

        const getMyNotifications = async () => {
          await view(q)
            .then(response => {
              setNotifications(response.data);
            })
            .catch(error => {
              console.log(error);
            })
        }

        getMyNotifications();
      }
    }

  }, [notifications, page, session.data._id, session.data.role])

  return (
    <section className="Notifications">
      <div className="container">
        <div className={session.data.role === "applicant" || session.data.role === "scholar" ? "" : "Notifications-grid"}>

          <div>
            {notifications.length ?
              notifications.map((item, index) => <div className="panel" key={index}><NotificationItem data={item} session={session} page={page} setEditId={setEditId} setNotifications={setNotifications} /></div>)
              :
              <small className="color-gray">NOTHING TO SHOW</small>
            }
          </div>

          {session.data.role === "applicant" || session.data.role === "scholar" ?
            undefined
            :
            <Form setNotifications={setNotifications} editId={editId} setEditId={setEditId} />
          }
        </div>
      </div>
    </section >
  );
};

export default Notifications;