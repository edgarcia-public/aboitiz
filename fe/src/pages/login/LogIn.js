import "../../assets/css/signuplogin.css";
import loginImage from '../../assets/images/utilities/login.jpg';
import { useContext, useState } from "react";
import { GlobalContext } from "../../App";
import { logInUser } from "../../helpers/endpoints/users";
import { checkObjValues } from "../../helpers/checkObjVals";
import { useNavigate } from "react-router-dom";
import { checkSession } from "../../helpers/checkSession";

const LogIn = () => {
  const { setSession, setNotif } = useContext(GlobalContext);
  const [userData, setUserData] = useState({ email: "", password: "" });
  const session = checkSession();
  const navigate = useNavigate();

  const handleInput = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    // check for current session
    if (session.success) return setNotif("You are currently logged in.");

    // check all object values
    if (!checkObjValues(userData)) return setNotif("All fields are required.");

    await logInUser(userData)
      .then(response => {

        if (response.extra === "new_admin") {
          setNotif("Log in successful. Please reset your password. Redirecting...");
          const sessionData = {
            success: true,
            data: {
              _id: response.data._id,
              new_admin: response.data.new_admin,
              role: response.data.role,
              status: response.data.status,
              due: new Date().getDate()
            }
          }

          setSession(sessionData);
          localStorage.setItem("session", JSON.stringify(sessionData));
          setTimeout(() => {
            window.location.href = "/users/reset-password"
          }, 3000);
        } else {
          const sessionData = {
            success: true,
            data: {
              _id: response.data._id,
              role: response.data.role,
              status: response.data.status,
              new_admin: response.data.new_admin,
              due: new Date().getDate()
            }
          }

          setSession(sessionData);
          localStorage.setItem("session", JSON.stringify(sessionData));

          // redirect based on role
          if ((sessionData.data.role === "super" || sessionData.data.role === "admin" || sessionData.data.role === "officer") && sessionData.data.status === "active") return navigate("/dashboard");

          return navigate("/dashboard/profile");
        }
      })
      .catch(error => {
        const response = error?.response.data;
        setNotif(response.data);

        // remove existing session
        localStorage.removeItem("session");

      })
  }

  return (
    <section className="LogIn">
      <div className="container">
        <div className="LogIn-flex">
          <div className="LogIn-flex-left">
            <img src={loginImage} className="flex-left-image" alt="Aboitiz_Login" />
          </div>

          <div className="LogIn-flex-right">
            <h3>Howdy, welcome back!</h3>
            <form onSubmit={handleSubmit}>
              <div className="input-box">
                <input type="text" name="email" placeholder="Email Address" onChange={handleInput} />
              </div>
              <div className="input-box">
                <input type="password" name="password" placeholder="Password" onChange={handleInput} />
              </div>
              <button type="submit" className="btn-theme">Log In</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default LogIn;
