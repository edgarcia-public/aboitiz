import { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { viewAllForms } from "../../helpers/endpoints/applications";
import { NavLink } from "react-router-dom";
import Loading from "../../components/Loading";
import HTMLTable from "../../components/html-table/HTMLTable";
import formatDatefromUTC from "../../helpers/formatDatefromUTC";
import removeChar from "../../helpers/removeChar";
import capitalize from "../../helpers/capitalize";

const Applications = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);
	const [loading, setLoading] = useState(true);
	const [tableData, setTableData] = useState("");

	useEffect(() => {
		if (!tableData) {
			const q = { user_id: session.data._id }
			const getMyApplications = async () => {
				await viewAllForms(q)
					.then(response => {
						if (response.data.length) {
							setNotif("Displaying data...");

							setTableData(makeTableData(response.data));
						} else {
							setNotif("No data found.");
							setTableData([]);
						}
						setLoading(false);
					})
					.catch(error => {
						const response = error.response.data;
						setTableData(response);
						setLoading(false);
					})
			}
			getMyApplications();
		}

		function makeTableData(data) {
			let tableData = [];

			data.map((obj, index) => tableData[index] = {
				applicant: obj.form_details.first_name + " " + obj.form_details.middle_name + " " + obj.form_details.last_name,
				scholarship: obj.cfa_id.title,
				form:
					<>
						<NavLink to={`/applications/forms/${obj.form_title}/${obj.cfa_id._id}/${obj.user_id._id}`} className="text-capitalize" target="_new">{removeChar(obj.form_title, "_-")}</NavLink>

						{obj.form_details?.thirdParty_forms && obj.form_details.thirdParty_forms.length ?
							<div className="third-party-forms-list">
								<label className="elem-block margin-bottom-10">3rd Party Forms</label>
								{obj.form_details.thirdParty_forms.map((url, index) =>
									<a
										key={index}
										href={url}
										target="_blank" rel="noopener noreferrer"
										className="third-party-forms-item"
									>
										Link {index + 1}
									</a>)
								}
							</div>
							:
							undefined
						}
					</>,
				date_submitted: formatDatefromUTC(obj.created_at),
				status: capitalize(obj.status) + " (" + formatDatefromUTC(obj.updated_at) + ")"
			})
			return tableData;
		}
	}, [tableData, session.data._id, session.data.role, setNotif]);

	return (
		<section className="Applications">
			<div className="container">
				{tableData.length ? <HTMLTable tableData={tableData} /> : undefined}
			</div>

			{loading ? <Loading /> : undefined}
		</section>
	);
};

export default Applications;
