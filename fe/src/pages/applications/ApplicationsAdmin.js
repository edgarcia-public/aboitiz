import { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { updateStatus, viewAllForms } from "../../helpers/endpoints/applications";
import { NavLink } from "react-router-dom";
import Loading from "../../components/Loading";
import HTMLTable from "../../components/html-table/HTMLTable";
import formatDatefromUTC from "../../helpers/formatDatefromUTC";
import removeChar from "../../helpers/removeChar";
import capitalize from "../../helpers/capitalize";

const ApplicationsAdmin = () => {
  const session = checkSession();
  const { setNotif } = useContext(GlobalContext);
  const [loading, setLoading] = useState(true);
  const [pending, setPending] = useState("");
  const [review, setReview] = useState("");
  const [shortlist, setShortlist] = useState("");
  const [approve, setApprove] = useState("");
  const [reject, setReject] = useState("");
  const [showUi, setShowUi] = useState(false);

  useEffect(() => {
    if (!pending) {
      getAllPending();
    }

    async function getAllPending() {
      await viewAllForms({ status: "pending" })
        .then(response => {
          if (response.data.length) {
            setNotif("Displaying data...");

            setPending(makeTableData(response.data));
          } else {
            setNotif("No data found.");

            setPending([]);
          }

          setLoading(false);

          if (!review) getAllReview();

          if (!shortlist) getAllShortlist();

          if (!approve) getAllApprove();

          if (!reject) getAllReject();
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
    }

    async function getAllReview() {
      await viewAllForms({ status: "review" })
        .then(response => {
          setReview(makeTableData(response.data))

          setLoading(false);
        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
    }

    async function getAllShortlist() {
      await viewAllForms({ status: "shortlist" })
        .then(response => {
          setShortlist(makeTableData(response.data))

        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
    }

    async function getAllApprove() {
      await viewAllForms({ status: "approve" })
        .then(response => {
          setApprove(makeTableData(response.data))

        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
    }

    async function getAllReject() {
      await viewAllForms({ status: "reject" })
        .then(response => {
          setReject(makeTableData(response.data))

        })
        .catch(error => {
          console.log(error);
          setLoading(false);
        })
    }

    async function handleAction(e) {
      // double check session!
      if (session.status) {
        setNotif(session.data);
        setTimeout(() => {
          window.location.href = "/users/log-in";
        }, 3000)
      }

      const userId = e.target.id;
      const status = e.target.parentElement.children[0].value;

      if (!status) return setNotif("Please select a Status.");

      const formData = {
        _id: userId,
        status: status
      }

      await updateStatus(formData)
        .then(response => {
          console.log(response);
          setNotif("Successful. Updating Applications data...");
          console.log(response.extra);

          setPending(makeTableData(response.extra.pending));
          setReview(makeTableData(response.extra.review));
          setShortlist(makeTableData(response.extra.shortlist));
          setApprove(makeTableData(response.extra.approve));
          setReject(makeTableData(response.extra.reject));
        })
        .catch(error => {
          setNotif(error);
        })
    }

    // assembles the data to pass to the HTMLTable
    function makeTableData(data) {
      let tableData = [];

      data.map((obj, index) => tableData[index] = {
        applicant: obj.form_details.first_name + " " + obj.form_details.middle_name + " " + obj.form_details.last_name,
        location: capitalize(obj.form_details.city) + ", " + capitalize(obj.form_details.province),
        scholarship: obj.cfa_id.title,
        form:
          <>
            <NavLink to={`/applications/forms/${obj.form_title}/${obj.cfa_id._id}/${obj.user_id._id}`} className="text-capitalize" target="_new">{removeChar(obj.form_title, "_-")}</NavLink>

            {obj.form_details?.thirdParty_forms && obj.form_details.thirdParty_forms.length ?
              <div className="third-party-forms-list">
                <label className="elem-block margin-bottom-10">3rd Party Forms</label>
                {obj.form_details.thirdParty_forms.map((url, index) =>
                  <a
                    key={index}
                    href={url}
                    target="_blank" rel="noopener noreferrer"
                    className="third-party-forms-item"
                  >
                    Link {index + 1}
                  </a>)
                }
              </div>
              :
              undefined
            }
          </>,
        date_submitted: formatDatefromUTC(obj.created_at),
        status: capitalize(obj.status),
        actions: <div className="table-action-btns">
          <select className="elem-block" name="status">
            <option value="">Status</option>
            {obj.status === "review" ? undefined : <option value="review">Review</option>}
            {obj.status === "shortlist" ? undefined : <option value="shortlist">Shortlist</option>}
            {obj.status === "approve" ? undefined : <option value="approve">Approve</option>}
            {obj.status === "reject" ? undefined : <option value="reject">Reject</option>}
          </select>
          <button className="statusButton" id={obj._id} onClick={handleAction}>SAVE</button>
        </div>
      })

      return tableData;
    }
  }, [review, pending, shortlist, approve, reject, session.data, session.status, setNotif, showUi]);



  return (
    <section className="Applications">
      <div className="container">

        {pending.length ?
          <>
            <h4>All Applications</h4>
            <HTMLTable tableData={pending} />
            <div className="division"></div>
          </>
          :
          undefined
        }

        {review.length ?
          <>
            <h4>All On Review</h4>
            <HTMLTable tableData={review} />
            <div className="division"></div>
          </>
          :
          undefined
        }

        {shortlist.length ?
          <>
            <h4>All Shortlisted</h4>
            <HTMLTable tableData={shortlist} />
            <div className="division"></div>
          </>
          :
          undefined
        }

        {approve.length ?
          <>
            <h4>All Approved</h4>
            <HTMLTable tableData={approve} />
            <div className="division"></div>
          </>
          :
          undefined
        }

        {reject.length ?
          <>
            <h4>All Rejected</h4>
            <HTMLTable tableData={reject} />
          </>
          :
          undefined
        }
      </div>

      {loading ? <Loading /> : undefined}
    </section>
  );
};

export default ApplicationsAdmin;
