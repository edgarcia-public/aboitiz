import "../../assets/css/signuplogin.css";
import { useContext, useState } from "react";
import { checkObjValues } from "../../helpers/checkObjVals"
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { resetPassword } from "../../helpers/endpoints/users";

const ResetPassword = () => {
  const session = checkSession();
  const { setNotif } = useContext(GlobalContext);
  const [userData, setUserData] = useState({
    email: "",
    currentPassword: "",
    newPassword: "",
    retry: ""
  });

  const handleInput = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    // password requirement > 5 characters
    if (userData.newPassword.length <= 5) return setNotif("The Password should be 6 or more characters.");

    // check confirm password
    if (userData.newPassword.toString() !== userData.retry.toString()) return setNotif("The Password and Confirm Password didn't match.");

    if (!checkObjValues(userData)) return setNotif("All inputs are required.");

    const doResetPassword = async () => {
      await resetPassword({ ...userData, new_admin: session.data?.new_admin })
        .then(response => {
          if (response.success) {
            localStorage.removeItem("session");

            const sessionData = {
              success: true,
              data: {
                _id: response.data._id,
                role: response.data.role,
                status: response.data.status,
                new_admin: response.data.new_admin,
                due: new Date().getDate()
              }
            }

            localStorage.setItem("session", JSON.stringify(sessionData));
            setNotif("Reset password successful. Redirecting...");

            setTimeout(() => {
              window.location.href = "/dashboard/profile";
            }, 3000);

          } else {
            setNotif("Something went wrong. Please refresh the page and try again.");
          }
        })
        .catch(error => {
          const response = error.response.data;
          setNotif(response.data);
        })
    }
    doResetPassword();
  }

  return (
    <section className="ResetPassword">
      <div className="container">
        <div className="panel">
          <div className="panel-header bg-blue">Reset Password</div>
          <div className="panel-body">
            <form onSubmit={handleSubmit}>
              <div className="grid grid-two gap-15">
                <div className="input-box">
                  <label className="elem-block margin-bottom-10">Email Address</label>
                  <input type="email" name="email" required onChange={handleInput} />
                </div>

                <div className="input-box">
                  <label className="elem-block margin-bottom-10">Current Password</label>
                  <input type="password" name="currentPassword" required onChange={handleInput} />
                </div>
              </div>

              <div className="grid grid-two gap-15 margin-bottom-15">
                <div className="input-box nogaps">
                  <label className="elem-block margin-bottom-10">Password</label>
                  <input type="password" name="newPassword" required onChange={handleInput} />
                </div>

                <div className="input-box">
                  <label className="elem-block margin-bottom-10">Confirm Password</label>
                  <input type="password" name="retry" required onChange={handleInput} />
                </div>
              </div>

              <button className="btn-theme">Reset Password</button>
            </form>
          </div>
          {session.data.role === "admin" || session.data.role === "officer" ?
            <div className="panel-footer">
              <label className="elem-block margin-bottom-10 color-theme">For new Admin or Officer:</label>
              In the Current Password field please enter the Default Password provided by your Admin.
            </div>
            :
            undefined
          }

        </div>
      </div>
    </section>
  )
};

export default ResetPassword;
