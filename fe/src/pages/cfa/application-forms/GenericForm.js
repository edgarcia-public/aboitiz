import "../../../assets/css/forms.css";
import { useContext } from "react";
import { loc } from "../../../assets/loc";
import { checkSession } from "../../../helpers/checkSession";
import { GlobalContext } from "../../../App";
import Loading from "../../../components/Loading";
import capitalize from "../../../helpers/capitalize";

const GenericForm = ({ formData, setFormData, handleSaveForm }) => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);

	const handleInput = (e) => {
		if (e.target.name === "region") return setFormData({ ...formData, region: e.target.value, province: "", city: "", town: "" });
		if (e.target.name === "province") return setFormData({ ...formData, province: e.target.value, city: "", town: "" });
		if (e.target.name === "city") return setFormData({ ...formData, city: e.target.value, town: "" });
		if (e.target.name === "town") return setFormData({ ...formData, town: e.target.value });
		if (e.target.name === "existing_student") setFormData({ ...formData, existing_student: e.target.value });
		if (e.target.name === "study_location") setFormData({ ...formData, study_location: e.target.value });

		setFormData({ ...formData, [e.target.name]: e.target.value });

		if (e.target.hasAttribute("next_id")) {
			const next_id = e.target.getAttribute("next_id");
			const nextParent = document.getElementById(next_id);
			const thisParent = e.target.parentElement.closest('.panel-step');

			thisParent.classList.add("hidden");
			nextParent.classList.remove("hidden");
		}

		if (e.target.hasAttribute("prev_id")) {
			const prev_id = e.target.getAttribute("prev_id");
			const prevParent = document.getElementById(prev_id);
			const thisParent = e.target.parentElement.closest('.panel-step');

			thisParent.classList.add("hidden");
			prevParent.classList.remove("hidden");
		}
	}

	const handleSubmit = (e) => {
		e.preventDefault();

		// double check session
		if (!session.success) {
			setNotif("You need an active account to submit the application form.");
			localStorage.removeItem("session");
			window.location.href = "/users/log-in";
		}

		const arrangeData = {
			first_name: formData.first_name,
			middle_name: formData.middle_name,
			last_name: formData.last_name,
			email: formData.email,
			mobile_number: formData.mobile_number,
			alternate_number: formData.alternate_number,
			address: formData.address,
			town: formData.town,
			city: formData.city,
			province: formData.province,
			region: formData.region,
			bday: formData.bday,
			elementary_school: formData.elementary_school,
			elementary_graduated: formData.elementary_graduated,
			junior_school: formData.junior_school,
			senior_school: formData.senior_school,
			senior_graduated: formData.senior_graduated,
			...formData
		}

		// removes undefined object keys
		delete arrangeData[""];

		setFormData(arrangeData);

		handleSaveForm(arrangeData);
	}

	if (!formData) return <Loading />

	return (
		<section className="ApplicationForm">
			<div className="container">
				<div className="ApplicationForm-stepper">
					<div className="panel panel-step">
						<div className="panel-header bg-blue">
							<p className="nogaps">DATA PRIVACY NOTICE AND CONSENT</p>
						</div>
						<div className="panel-body">
							<p>ABOITIZ FOUNDATION, INC. is committed to protecting the privacy of its data subjects, and ensuring the safety and security of personal data under its control and custody. This policy provides information on what personal data is gathered by the the company about its current, past, and prospective students; how it will use and process this; how it will keep this secure; and how it will dispose of it when it is no longer needed. This information is provided in compliance with the Philippine Republic Act No. 10173, also known as, the Data Privacy Act of 2012 (DPA) and its Implementing Rules and Regulations (DPA-IRR). It sets out Aboitiz Foundation's data protection practices designed to safeguard the personal data of individuals it deals with, and also to inform such individuals of their rights under the Act.</p>

							<p>This Data Privacy Notice and Consent Form may be amended at any time without prior notice, and such amendments will be notified to you via email.</p>

							<p><b>PRIVACY NOTICE</b></p>

							<p><b>Information Collected</b></p>

							<p>Aboitiz Foundation, Inc. collects, stores, and processes personal data from its current, past and prospective scholars, starting with the information provided at application through to information collected throughout the whole course of his scholarship. This will include:</p>

							<ul>
								<li>Contact information, such as, name, addresses, telephone numbers, email addresses and other contact details</li>
								<li>Personal information, such as date of birth, educational background, awards received, leadership experience, and trainings attended</li>
							</ul>

							<p><b>Use of Information</b></p>

							<p>The collected personal data is used solely for the following purpose of processing evaluation of the scholarship application.</p>

							<p><b>Information Sharing</b></p>

							<p>Personal data under the custody of Aboitiz Foundation, Inc. shall be disclosed only to authorized recipients of such data. Otherwise, we will share your personal data with third parties, other than your parents and/or guardian on record for minors, only with your consent, or when required or permitted by our policies and applicable law, such as with service providers who perform services for us and help us evaluate your application.</p>

							<p>If you have a concern or complaint about the way we are collecting or using your personal data, you may reach us through our email at aboitizfoundation@aboitiz.com</p>
						</div>
						<div className="panel-footer text-center">
							<button className="btn-green" name="privacy_agree" value="true" next_id="program" onClick={handleInput}>I AGREE</button>
						</div>
					</div>

					<div className="panel panel-step hidden" id="program">
						<div className="panel-header bg-blue">
							<p className="nogaps">ABOITIZ COLLEGE SCHOLARSHIP PROGRAM 2023</p>
						</div>
						<div className="panel-body">
							<p>Thank you for your interest in Aboitiz College Scholarship Program. Please read carefully and familiarize yourself with the program eligibility, the application requirements, and the screening and selection process.</p>

							<p><b>PROGRAM ELIGIBILITY</b></p>

							<p>To qualify for the Aboitiz College Scholarship, the applicant must meet the following:</p>

							<ul>
								<li>A full-time incoming freshman of an eligible college or university</li>
								<li>Pursue a degree identified by Aboitiz Foundation;</li>
								<li>Attain a general weighted average (GWA) of at least 88% with no failing and incomplete grade;</li>
								<li>Not have a record of any form of disciplinary action; and</li>
								<li>Accomplish the application form and submit the requirements on or before June 30, 2023.</li>
							</ul>

							<p><b>REQUIREMENTS</b></p>

							<p>Please prepare the following as requirements of your college scholarship application:</p>

							<ul>
								<li>Grade 12 Report Card reflecting your first semester grades (First and Second Quarter) or second semester grades whichever is available;</li>
								<li>Proof of awards and extracurricular involvement.</li>
							</ul>

							<p><b>SCREENING & SELECTION PROCESS</b></p>

							<p>Applications will be evaluated by Aboitiz Foundation, Inc.</p>

							<ul>
								<li>Shortlisted applicants will be advised to undergo a panel interview to be conducted by Aboitiz Foundation, Inc.</li>
								<li>Applicants who passed the interview will take an exam which would be conducted by a third-party provider.</li>
								<li>Successful applicants will be notified via phone or email.</li>
							</ul>

							<p>Applicants should fully disclose during the application all active scholarships he/she is enjoying and/or applications for other scholarships he/she is actively pursuing. No scholar shall enjoy two or more scholarships may it be within the group or external except for Academic Scholarships offered by the institution where the scholar is enrolled. Scholars who opt for the scholarship other than the program discussed in this document shall be disqualified for Aboitiz Foundation, Inc.'s Scholarship Program.</p>
						</div>
						<div className="panel-footer text-center">
							<button next_id="partOne" className="btn-green" onClick={handleInput}>NEXT</button>
						</div>
					</div>

					<div className="panel panel-step hidden" id="partOne">
						<div className="panel-header bg-blue">
							<p className="nogaps">PART ONE: PERSONAL INFORMATION</p>
						</div>

						<div className="panel-body">
							<p>Please ensure to fill out all of the fields below.</p>
							<div className="form-item">
								<div>
									<label>Are you an existing college student?</label>
								</div>
								<div>
									<p>
										<input type="radio" name="existing_student"
											value="yes"
											checked={formData.existing_student === "yes"}
											required onChange={handleInput} /> Yes
									</p>
									<p>
										<input type="radio" name="existing_student"
											value="no"
											checked={formData.existing_student === "no"}
											required onChange={handleInput} /> No
									</p>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Full Name</label>
								</div>
								<div className="grid grid-three gap-15">
									<div className="input-box">
										<input type="text" className="text-capitalize" name="first_name" placeholder="Juan"
											value={formData.first_name || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="text" className="text-capitalize" name="middle_name" placeholder="Santos"
											value={formData.middle_name || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="text" className="text-capitalize" name="last_name" placeholder="Dela Cruz"
											value={formData.last_name || ""}
											required onChange={handleInput} />
									</div>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Address</label>
								</div>
								<div>
									<div className="input-box">
										<input type="text" name="address" placeholder="House number Street Subdivision Village"
											value={formData.address || ""}
											onChange={handleInput} required />
									</div>

									<div className="grid grid-two gap-15">
										<div className="input-box">
											<select name="region" value={formData.region || "label"}
												onChange={handleInput} required>
												<option value="label" disabled>Select Region</option>
												{Object.keys(loc).map((e, i) => <option key={i} value={e.toString()}>{loc[e].region_name}</option>)}
											</select>
										</div>

										<div className="input-box">
											<select name="province" value={formData.province || "label"}
												disabled={formData.region ? false : true}
												onChange={handleInput} required>

												<option value="label" disabled>Select Province</option>

												{formData.region ?
													Object.keys(loc[formData.region]
														.province_list).map((e, i) => <option key={i} value={e}>{e}</option>)
													:
													<option value="label" disabled>Select Province</option>
												}
											</select>
										</div>
									</div>
									<div className="grid grid-two gap-15">
										<div className="input-box">
											<select name="city" value={formData.city || "label"}
												disabled={formData.province ? false : true}
												onChange={handleInput} required>

												<option value="label" disabled>Select City</option>

												{formData.province ?
													Object.keys(loc[formData.region]
														.province_list[formData.province]
														.municipality_list).map((e, i) => <option key={i} value={e}>{e}</option>)
													:
													<option value="label">Select Municipality</option>
												}
											</select>
										</div>
										<div className="input-box">
											<select name="town" value={formData.town || "label"} disabled={formData.city ? false : true}
												onChange={handleInput} required>
												<option value="label" disabled>Select Town</option>

												{formData.city ?
													loc[formData.region]
														.province_list[formData.province]
														.municipality_list[formData.city]
														.barangay_list.map((e, i) => <option key={i} value={e}>{e}</option>)
													:
													<option value="label">Select Municipality</option>
												}
											</select>
										</div>
									</div>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Mobile and Alternate Number</label>
								</div>
								<div className="grid grid-two gap-15">
									<div className="input-box">
										<input type="text" name="mobile_number" placeholder="0918 123 4567"
											value={formData.mobile_number || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="text" name="alternate_number" placeholder="0917 123 4567"
											value={formData.alternate_number || ""}
											required onChange={handleInput} />
									</div>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Email Address and Birthdate</label>
								</div>
								<div className="grid grid-two gap-15">
									<div className="input-box">
										<input type="email" name="email" placeholder="juandelacruz@gmail.com"
											value={formData.email || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="date" name="bday" placeholder="Birthdate"
											value={formData.bday || ""}
											required onChange={handleInput} />
									</div>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Where do you plan to study?</label>
								</div>

								<div className="grid grid-two">
									<div className="input-box">
										<select name="study_location" value={formData.study_location || "label"}
											onChange={handleInput} required>
											<option value="label">Select Region</option>
											<option value="Region_I">Region I</option>
											<option value="Region_II">Region II</option>
											<option value="Region_III">Region III</option>
											<option value="NCR">NCR</option>
											<option value="Region_IVA">Region IVA</option>
											<option value="Region_IVB">Region IVB</option>
											<option value="Region_V">Region V</option>
											<option value="Region_VI">Region VI</option>
											<option value="Region_VII">Region VII</option>
											<option value="Region_VIII">Region VIII</option>
											<option value="Region_IX">Region IX</option>
											<option value="Region_X">Region X</option>
											<option value="Region_XI">Region XI</option>
											<option value="Region_XII">Region XII</option>
											<option value="BARMM">BARMM</option>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div className="panel-footer text-center">
							<button prev_id="program" onClick={handleInput}>BACK</button>&nbsp;&nbsp;<button next_id="partTwo" className="btn-green" onClick={handleInput}>NEXT</button>
						</div>
					</div>

					<div className="panel margin-bottom-15 panel-step hidden" id="partTwo">
						<div className="panel-header bg-blue">
							<p className="nogaps">PART TWO: EDUCATIONAL BACKGROUND</p>
						</div>

						<div className="panel-body">
							<div className="form-item">
								<div>
									<label>Elementary and Year Graduated</label>
								</div>
								<div className="grid grid-two gap-15">
									<div className="input-box">
										<input type="text" name="elementary_school"
											value={formData.elementary_school || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="date" name="elementary_graduated"
											value={formData.elementary_graduated || ""}
											required onChange={handleInput} />
									</div>
								</div>
							</div>

							<div className="form-item">
								<div>
									<label>Junior and Year Graduated</label>
								</div>
								<div className="grid grid-two gap-15">
									<div className="input-box">
										<input type="text" name="junior_school"
											value={formData.junior_school || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="date" name="junior_graduated"
											value={formData.junior_graduated || ""}
											required onChange={handleInput} />
									</div>
								</div>
							</div>

							<small className="elem-block margin-bottom-10"><i>For applicants who are currently enrolled in senior high school, kindly indicate your expected year of graduation.</i></small>

							<div className="form-item">
								<div>
									<label>Senior High School, Year Graduated and General Weighted Average (GWA)</label>
								</div>
								<div className="grid grid-three gap-15">
									<div className="input-box">
										<input type="text" name="senior_school"
											value={formData.senior_school || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="date" name="senior_graduated"
											value={formData.senior_graduated || ""}
											required onChange={handleInput} />
									</div>
									<div className="input-box">
										<input type="text" name="senior_gwa"
											value={formData.senior_gwa || ""}
											placeholder="GWA" required onChange={handleInput} />
									</div>
								</div>
							</div>
						</div>

						<div className="panel-footer text-center">
							<button prev_id="partOne" onClick={handleInput}>BACK</button>
							{session.data.role === "applicant" && (formData.status === "pending" || !formData.status) ?
								<>
									<span style={{ padding: "0 5px" }}></span>
									<button className={formData.status ? "btn" : "btn-green"} onClick={handleSubmit}>
										{formData.status ? "UPDATE" : "SUBMIT"}
									</button>
								</>
								:
								undefined
							}
						</div>
					</div>
				</div>

				{formData.status ? <p>Status: {capitalize(formData.status)}</p> : undefined}
			</div>
		</section >
	)
}

export default GenericForm;