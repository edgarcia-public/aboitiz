import "../../assets/css/templates.css";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { viewOneCfa } from "../../helpers/endpoints/cfa";
import NotFound from "../error/NotFound";
import DOMpurify from "dompurify";
import capitalize from "../../helpers/capitalize";
import { checkSession } from "../../helpers/checkSession";

const CFALanding = () => {
	const { cfa_id } = useParams();
	const [cfa, setCfa] = useState();
	const session = checkSession();

	useEffect(() => {
		const getCfa = async () => {
			if (!cfa) {
				await viewOneCfa(cfa_id)
					.then(response => {
						setCfa(response.data);
					})
					.catch(error => {
						console.log(error);
					})
			}
		}

		getCfa();
	}, [cfa, cfa_id])

	if (cfa) {
		const parser = new DOMParser();
		const doc = parser.parseFromString(cfa.canvas, "text/html").documentElement.outerHTML;
		const removeTitle = doc.replaceAll('title="CLICK TO EDIT"', "");
		const removeContentEditable = removeTitle.replaceAll('contenteditable="true"', "");
		const removeEditableClass = removeContentEditable.replaceAll("editable-element", "");
		let finalHtml;

		if (!session.success && cfa.status !== "open") return <NotFound />;

		if ((session.data.role === "applicant" || session.data.role === "scholar") &&
			(cfa.status === "draft" || cfa.status === "closed")) return <NotFound />;

		if (cfa.form === "external") {
			const hrefRegex = /href="(.*?)"/g;
			const updateUrl = removeEditableClass.replace(hrefRegex, `href="/applications/forms/basic/${cfa._id}"`);

			finalHtml = updateUrl;

			// applications/forms/:form_title/:cfa_id/:user_id?
		} else {
			finalHtml = removeEditableClass;
		}

		document.title = `${capitalize(cfa.title)} | Aboitiz`;
		return (
			<section className="Landing">
				<div className="container">
					{cfa.status === "draft" || cfa.status === "closed" ?
						<div className="text-center margin-bottom-15">
							<label>CFA Status:</label> <span>{capitalize(cfa.status)}</span>
						</div>
						:
						undefined
					}
					<span dangerouslySetInnerHTML={{ __html: DOMpurify.sanitize(finalHtml) }} />
				</div>
			</section>
		)
	} else {
		return <div className="container"><p>Loading page...</p></div>
	}
};

export default CFALanding;
