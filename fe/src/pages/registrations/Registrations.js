import { useContext, useEffect, useState } from "react";
import { checkSession } from "../../helpers/checkSession";
import { GlobalContext } from "../../App";
import { updateStatus, viewStatus } from "../../helpers/endpoints/users";
import HTMLTable from "../../components/html-table/HTMLTable";
import Loading from "../../components/Loading";
import formatDatefromUTC from "../../helpers/formatDatefromUTC";
import capitalize from "../../helpers/capitalize";

const Registrations = () => {
  const session = checkSession();
  const { setNotif } = useContext(GlobalContext);
  const [newUsers, setNewUsers] = useState("");
  const [allUsers, setAllUsers] = useState("");

  if (session.data.new_admin) window.location.href = "/users/reset-password";

  if (session.data.role === "applicant" || session.data.role === "scholar") {
    setNotif("Unauthorized.");
    window.location.href = "/dashboard/profile";
  }

  useEffect(() => {
    if (!newUsers) {
      const getAllNewSignUps = async () => {
        await viewStatus({ status: "pending" })
          .then(response => {
            if (response.data.length) {
              setNotif("Registrations data...");

              setNewUsers(makeTableData(response.data));
            } else {
              setNotif("No data found.");

              setNewUsers([]);
            }

            if (!allUsers) {
              getAllUsers();
            }
          })
          .catch(error => {
            const response = error.response.data;
            setNotif(response.data);
          })
      }

      getAllNewSignUps();
    }

    async function getAllUsers() {
      await viewStatus({ status: { $in: ["active", "reject"] } })
        .then(response => {
          setNotif("Displaying Registrations data...");

          setAllUsers(makeTableData(response.data));
        })
        .catch(error => {
          const response = error.response.data;
          setNotif(response.data);
        })
    }

    async function handleClick(e) {

      // double check session!
      if (session.status) {
        setNotif(session.data);
        setTimeout(() => {
          window.location.href = "/users/log-in";
        }, 3000)
      }

      const userId = e.target.id;
      const status = e.target.parentElement.children[0].value;

      if (!status) return setNotif("Please select a Status.");

      const userData = {
        _id: userId,
        status: status
      }

      await updateStatus(userData, { status: "pending" })
        .then(response => {
          setNotif("Successful. Updating Registrations data...");

          setNewUsers(makeTableData(response.data));
          getAllUsers();
        })
        .catch(error => {
          const response = error.response.data
          setNotif(response.data);
        })
    }

    // assembles the data to pass to the HTMLTable
    function makeTableData(data) {
      let tableData = [];

      data.map((obj, index) => tableData[index] = {
        full_name: obj.first_name + " " + obj.middle_name + " " + obj.last_name + " " + (obj._id === session.data._id ? "(You)" : ""),
        email: obj.email,
        role: capitalize(obj.role),
        status: capitalize(obj.status),
        sign_up_date: formatDatefromUTC(obj.created_at),
        actions: (session.data.role === "super" && obj.role === "admin") ||
          (session.data.role === "super" && obj.role === "officer") ||
          (session.data.role === "super" && obj.role === "applicant") ||
          (session.data.role === "super" && obj.role === "scholar") ||
          (session.data.role === "admin" && obj.role === "officer") ||
          (session.data.role === "admin" && obj.role === "applicant") ||
          (session.data.role === "admin" && obj.role === "scholar") ||
          (session.data.role === "officer" && obj.role === "applicant") ||
          (session.data.role === "officer" && obj.role === "scholar")
          ?
          <div className="table-action-btns">
            <select className="elem-block" name="status">
              <option value="">Status</option>
              {obj.status === "active" ? undefined : <option value="active">Active</option>}
              {obj.status === "reject" ? undefined : <option value="reject">Reject</option>}
              {obj.status === "pending" ? undefined : <option value="pending">Pending</option>}
            </select>
            <button className="statusButton" id={obj._id} onClick={handleClick}>SAVE</button>
          </div>
          :
          undefined
      })
      return tableData;
    }
  }, [newUsers, allUsers, setNotif, session.status, session.data]);

  if (newUsers === "pending") return <Loading />;

  return (
    <section>
      <div className="container">
        {newUsers.length > 0 ?
          <>
            <h4>New Sign Ups</h4>
            <HTMLTable tableData={newUsers} />
            <div className="division"></div>
          </>
          :
          undefined
        }

        {allUsers.length > 0 ?
          <>
            <h4>Active and Rejected Users</h4>
            <HTMLTable tableData={allUsers} />
          </>
          :
          undefined
        }
      </div>
    </section>
  );
};

export default Registrations;
