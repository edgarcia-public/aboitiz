import "../../assets/css/templates.css";
import { useContext, useEffect, useState } from "react";
import { checkObjValues } from "../../helpers/checkObjVals";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { viewOneCfa, updateCfa } from "../../helpers/endpoints/cfa";
import { useParams } from "react-router-dom";
import dbDateConvert from "../../helpers/dbDateConvert";
import Loading from "../../components/Loading";

const UpdateCFALanding = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);
	const { crud, template_id } = useParams();
	const [cfaData, setCfaData] = useState({
		title: "",
		form: "",
		external: "",
		canvas: "",
		status: "open",
		author: session.data._id,
	});

	const handleInput = (e) => {
		if (e.target.name === "form") return setCfaData({ ...cfaData, form: e.target.value });
		if (e.target.name === "status") return setCfaData({ ...cfaData, status: e.target.value });

		setCfaData({ ...cfaData, [e.target.name]: e.target.value });
	};

	useEffect(() => {
		if (crud === "update" && !cfaData.title) {
			const getCfa = async () => {
				await viewOneCfa(template_id)
					.then((response) => {
						setCfaData(response.data);
					})
					.catch((error) => {
						console.log(error);
					});
			};

			getCfa();
		}
	}, [session, cfaData, template_id, crud, setCfaData]);

	const handleUpdate = (e) => {
		e.preventDefault();

		const template = document.getElementById("template-parent");
		const html = template.innerHTML.toString();
		let templateDom;

		if (cfaData.form === "external") {
			templateDom = changeHref(html, cfaData.external)
		} else {
			templateDom = changeHref(html, `/applications/forms/${cfaData.form}/${template_id}`)
		}

		// double check session
		if (!session.success) {
			setNotif("Session expired. Please log in.");
			localStorage.removeItem("session");
			window.location.href = "/users/log-in";
		}

		const data = { ...cfaData, canvas: templateDom }

		if (!checkObjValues(data)) return setNotif("Make sure both CFA Settings and the Template are set.");

		const udpateCfa = async () => {
			await updateCfa(data)
				.then((response) => {
					setCfaData(response.data);

					setNotif("CFA saved successfully.");
				})
				.catch((error) => {
					const response = error?.response.data;

					setNotif(response.data);
				});
		}
		udpateCfa();
	};

	if (!cfaData.title) return <Loading />;

	function changeHref(html, newUrl) {
		const hrefRegex = /href="(.*?)"/g;
		const updateUrl = html.replace(hrefRegex, `href="${newUrl}"`);
		return updateUrl;
	}

	function parseHtml() {
		const parser = new DOMParser();
		const doc = parser.parseFromString(cfaData.canvas, "text/html").documentElement.outerHTML;

		return (
			<span dangerouslySetInnerHTML={{ __html: `<div id="template-parent">${doc}</div>` }} />
		);
	}

	return (
		<section className="Templates">
			<div className="container">
				<div className="Templates-canvas">
					<div className="Templates-canvas-left">
						<div className="panel">
							<div className="panel-header bg-blue">CFA Settings</div>
							<div className="panel-body">
								<form onSubmit={handleUpdate}>
									<div className="input-box">
										<label>CFA Title</label>
										<input type="text" name="title" value={cfaData.title} onChange={handleInput} />
									</div>
									<div className="input-box">
										<label>Button Redirect</label>
										<select name="form" onChange={handleInput} value={cfaData.form === "external" ? "external" : cfaData.form}>
											<option value="label">Select a form</option>
											<option value="generic">Generic Form</option>
											<option value="external">External Link</option>
										</select>
									</div>
									{cfaData.form === "external" ?
										<div className="input-box">
											<label>External Link</label>
											<input type="text" name="external" value={cfaData.external} onChange={handleInput} />
										</div>
										:
										undefined
									}
									<div className="input-box">
										<label>Status</label>
										<select name="status" value={cfaData.status ? cfaData.status : "open"} onChange={handleInput}>
											<option value="open">Open</option>
											<option value="draft">Draft</option>
											<option value="closed">Closed</option>
										</select>
									</div>
									<button className="elem-block btn-green" onClick={handleUpdate}>UPDATE</button>
									<div className="cfa-details-container">
										<div className="margin-bottom-15"></div>
										<label className="elem-block margin-bottom-5"> URL Address </label>
										<a href={`/landing/cfa/${cfaData._id}`} className="text-link" target="_blank" rel="noreferrer"> View CFA </a>
										<div className="margin-bottom-15"></div>
										<label className="elem-block margin-bottom-5"> Published On </label>
										<p>{dbDateConvert(cfaData.created_at)}</p>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div className="Templates-canvas-right">
						<label className="margin-bottom-10 elem-block">CFA PREVIEW</label>
						{parseHtml()}
					</div>
				</div>
			</div>
		</section>
	);
};

export default UpdateCFALanding;
