import "../../assets/css/templates.css";
import { useContext, useState } from "react";
import { useParams } from "react-router-dom";
import { GlobalContext } from "../../App";
import { checkObjValues } from "../../helpers/checkObjVals";
import { checkSession } from "../../helpers/checkSession";
import { addCfa } from "../../helpers/endpoints/cfa";
import CollegeGraduates from "./template-items/CollegeGraduates";
import Loading from "../../components/Loading";
import NotFound from "../error/NotFound";
import GotYouCovered from "./template-items/GotYouCovered";
import Dream from "./template-items/Dream";

const CreateCFALanding = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);
	const { template_id } = useParams();
	let page;
	const [cfaData, setCfaData] = useState({
		title: "",
		form: "",
		external: "",
		canvas: "",
		status: "open",
		author: session.data._id,
	});

	const handleInput = (e) => {
		if (e.target.name === "form") return setCfaData({ ...cfaData, form: e.target.value })
		if (e.target.name === "status") return setCfaData({ ...cfaData, status: e.target.value });

		setCfaData({ ...cfaData, [e.target.name]: e.target.value });
	};

	// save new CFA
	const handleSubmit = (e) => {
		e.preventDefault();

		const template = document.getElementById("template-parent");
		const html = template.innerHTML.toString();
		let templateDom;

		if (cfaData.form === "external") {
			templateDom = changeHref(html, cfaData.external)
		} else {
			templateDom = changeHref(html, `/applications/forms/${cfaData.form}/${template_id}`);
		}

		// double check session
		if (!session.success) {
			setNotif("Session expired. Please log in.");
			localStorage.removeItem("session");
			window.location.href = "/users/log-in";
		}

		const data = { ...cfaData, canvas: templateDom }

		if (cfaData.form === "external") {
			if (!checkObjValues(data)) return setNotif("Make sure both CFA Settings and the Template are set.");
		} else {
			delete data.external

			if (!checkObjValues(data)) return setNotif("Make sure both CFA Settings and the Template are set.");
		}

		const newCfaData = { ...cfaData, canvas: templateDom }

		const saveCfa = async () => {
			await addCfa(newCfaData)
				.then((response) => {
					setNotif("CFA saved successfully.");

					window.location.href = `/dashboard/call-for-application/update/${response.data._id}`;
				})
				.catch((error) => {
					const response = error?.response.data;

					setNotif(response.data);
				});
		}
		saveCfa();
	}

	function changeHref(html, newUrl) {
		const hrefRegex = /href="(.*?)"/g;
		const updateUrl = html.replace(hrefRegex, `href="${newUrl}"`);
		return updateUrl;
	}

	switch (template_id) {
		case "college-graduates":
			page = <CollegeGraduates cfaData={cfaData} />
			break;
		case "got-you-covered":
			page = <GotYouCovered cfaData={cfaData} />
			break;
		case "dream":
			page = <Dream cfaData={cfaData} />
			break;
		default:
			page = <NotFound />
			break;
	}

	return (
		<section className="Templates">
			<div className="container">
				<div className="Templates-canvas">
					<div className="Templates-canvas-left">
						<div className="panel">
							<div className="panel-header bg-blue">CFA Settings</div>
							<div className="panel-body">
								<form onSubmit={handleSubmit}>
									<div className="input-box">
										<label>CFA Title</label>
										<input type="text" name="title" value={cfaData.title} onChange={handleInput} />
									</div>
									<div className="input-box">
										<label>Button Redirect</label>
										<select name="form" onChange={handleInput} value={cfaData.form ? cfaData.form : "label"}>
											<option value="label">Select a form</option>
											<option value="generic">Generic Form</option>
											<option value="external">External Link</option>
										</select>
									</div>
									{cfaData.form === "external" ?
										<div className="input-box">
											<label>External Link</label>
											<input type="text" name="external" value={cfaData.external} onChange={handleInput} />
										</div>
										:
										undefined
									}
									<div className="input-box">
										<label>Status</label>
										<select name="status" value={cfaData.status ? cfaData.status : "open"} onChange={handleInput}>
											<option value="open">Open</option>
											<option value="draft">Draft</option>
											<option value="closed">Closed</option>
										</select>
									</div>
									<button className="elem-block btn-green" onClick={handleSubmit}>SAVE</button>
								</form>
							</div>
						</div>
					</div>

					<div className="Templates-canvas-right">
						<label className="margin-bottom-10 elem-block">CFA PREVIEW</label>
						{template_id ? <div id="template-parent">{page}</div> : <Loading />}
					</div>
				</div>
			</div>
		</section>
	);
};

export default CreateCFALanding;
