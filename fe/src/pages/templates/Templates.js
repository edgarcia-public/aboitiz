import "../../assets/css/templates.css";
import { NavLink } from "react-router-dom";
import { checkSession } from "../../helpers/checkSession";
import { useContext } from "react";
import { GlobalContext } from "../../App";
import SubNavbar from "../../layouts/subnavbar/SubNavbar";
import Loading from "../../components/Loading";
import templateOne from "../../assets/images/templates/templateOne.PNG";
import dream from "../../assets/images/templates/dream/dream.PNG";
import gotyoucovered from "../../assets/images/templates/gotyoucovered/gotyoucovered.PNG";

const Templates = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);

	if (!session.success) {
		setNotif("session expired. Please log in.");
		window.location.href = "/users/log-in";
		return <Loading />;
	}

	if (session.data.role === "applicant" || session.data.role === "scholar") {
		setNotif("Unauthorized.");
		window.location.href = "/dashboard/profile";
		return <Loading />;
	}

	document.title = "CFA Templates | Aboitiz";
	return (
		<>
			<SubNavbar />
			<section className="Templates">
				<div className="container">
					<p className="margin-bottom-15">Select a Call For Application template.</p>

					<div className="grid grid-three templates-list">
						<div className="grid-item">
							<div className="template-item">
								<NavLink to={"/dashboard/call-for-application/create/college-graduates"} className="templates-item-link">
									<img src={templateOne} alt="" className="template-image" />
								</NavLink>
							</div>
						</div>

						<div className="grid-item">
							<div className="template-item">
								<NavLink to={"/dashboard/call-for-application/create/got-you-covered"} className="templates-item-link">
									<img src={gotyoucovered} alt="" className="template-image" />
								</NavLink>
							</div>
						</div>
						<div className="grid-item">
							<div className="template-item">
								<NavLink to={"/dashboard/call-for-application/create/dream"} className="templates-item-link">
									<img src={dream} alt="" className="template-image" />
								</NavLink>
							</div>
						</div>
					</div>
				</div>
			</section>
		</>
	)
}

export default Templates;