import checkIcon from "../../../assets/images/utilities/check.png";

const CollegeGraduates = ({ cfaData }) => {

	return (
		<div className="college-graduates">
			<div className="college-graduates-header">
				<div className="one-header-left"></div>
				<div className="one-header-right">
					<div className="header-right-item">
						<h2
							contentEditable
							suppressContentEditableWarning={true}
							className="heading-one editable-element" title="CLICK TO EDIT">
							Lorem Ipsum has been the industry's standard dummy text ever 1500s.
						</h2>
						<a
							contentEditable
							suppressContentEditableWarning={true}
							href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-theme margin-bottom-15 color-white">
							Apply Now
						</a>
						<h4
							contentEditable
							suppressContentEditableWarning={true}
							className="editable-element" title="CLICK TO EDIT">
							It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
						</h4>
					</div>
				</div>
			</div>
			<div className="college-graduates-banner">
				<p
					contentEditable
					suppressContentEditableWarning={true}
					className="editable-element text-center nogaps" title="CLICK TO EDIT">
					It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages.
				</p>
			</div>
			<div className="college-graduates-features">
				<h2
					contentEditable
					suppressContentEditableWarning={true}
					className="heading-one text-center editable-element margin-bottom-15" title="CLICK TO EDIT">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry.
				</h2>

				<br></br>

				<div className="grid grid-two gap-15 margin-bottom-15">
					<div className="one-features-item">
						<img src={checkIcon} alt="" />
						<p
							contentEditable
							suppressContentEditableWarning={true}
							className="editable-element" title="CLICK TO EDIT">Bacon ipsum dolor amet ham pork swine short loin chicken shank. Buffalo biltong ham frankfurter, hamburger brisket burgdoggen drumstick tongue tenderloin short ribs pancetta picanha shankle rump.
						</p>
					</div>
					<div className="one-features-item">
						<img src={checkIcon} alt="" />
						<p
							contentEditable
							suppressContentEditableWarning={true}
							className="editable-element" title="CLICK TO EDIT">Bacon ipsum dolor amet ham pork swine short loin chicken shank. Buffalo biltong ham frankfurter, hamburger brisket burgdoggen drumstick tongue tenderloin short ribs pancetta picanha shankle rump.
						</p>
					</div>
				</div>
				<div className="grid grid-two gap-15">
					<div className="one-features-item">
						<img src={checkIcon} alt="" />
						<p
							contentEditable
							suppressContentEditableWarning={true}
							className="editable-element" title="CLICK TO EDIT">Bacon ipsum dolor amet ham pork swine short loin chicken shank. Buffalo biltong ham frankfurter, hamburger brisket burgdoggen drumstick tongue tenderloin short ribs pancetta picanha shankle rump.
						</p>
					</div>
					<div className="one-features-item">
						<img src={checkIcon} alt="" />
						<p
							contentEditable
							suppressContentEditableWarning={true}
							className="editable-element" title="CLICK TO EDIT">Bacon ipsum dolor amet ham pork swine short loin chicken shank. Buffalo biltong ham frankfurter, hamburger brisket burgdoggen drumstick tongue tenderloin short ribs pancetta picanha shankle rump.
						</p>
					</div>
				</div>
			</div>
			<footer>
				<h2
					contentEditable
					suppressContentEditableWarning={true}
					className="heading-one text-center editable-element" title="CLICK TO EDIT">
					It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
				</h2>
				<a
					contentEditable
					suppressContentEditableWarning={true}
					href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-theme color-white">
					Apply Now
				</a>
			</footer>
		</div>
	)
}

export default CollegeGraduates;