import "../../../assets/css/gotyoucovered.css";

const GotYouCovered = ({ cfaData }) => {
	return (
		<section className="gotyoucovered template-max-width">
			<div className="hero">
				<h1
					contentEditable
					suppressContentEditableWarning={true}
					className="color-blue"
				>
					Aboitiz Got You Covered!
				</h1>
				<h4
					contentEditable
					suppressContentEditableWarning={true}
				>
					Bacon ipsum dolor amet ribeye swine bresaola ham hock leberkas rump sausage strip steak tongue. Chislic meatball shank t-bone, capicola alcatra shoulder tenderloin ground round shankle strip steak prosciutto.
				</h4>
				<a
					contentEditable
					suppressContentEditableWarning={true}
					href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-rounded btn-theme margin-bottom-15 color-white"
				>
					Apply Now
				</a>
			</div>

			<div className="content">
				<div className="content-item">
					<div className="content-item--title">
						<h2>WHY DO WE CARE</h2>
					</div>
					<div className="content-item-grid">
						<div className="content-item--left help"></div>
						<div className="content-item--right content-text">
							<p
								contentEditable
								suppressContentEditableWarning={true}
								className="nogaps"
							>
								Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
							</p>
						</div>
					</div>
				</div>

				<div className="content-item">
					<div className="content-item--title">
						<h2>HOW DO WE HELP</h2>
					</div>
					<div className="content-item-grid">
						<div className="content-item--left content-text">
							<p
								contentEditable
								suppressContentEditableWarning={true}
								className="nogaps"
							>
								Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
							</p>
						</div>
						<div className="content-item--right grad"></div>
					</div>
				</div>

				<div className="content-item">
					<div className="content-item--title">
						<h2>WHO ARE ABOITIZ</h2>
					</div>
					<div className="content-item-grid">
						<div className="content-item--left charity"></div>
						<div className="content-item--right content-text">
							<p
								contentEditable
								suppressContentEditableWarning={true}
								className="nogaps"
							>
								Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
							</p>
						</div>
					</div>
				</div>
			</div>

			<footer>
				<h1
					contentEditable
					suppressContentEditableWarning={true}
					className="color-blue"
				>
					Aboitiz Got You Covered!
				</h1>
				<a
					contentEditable
					suppressContentEditableWarning={true}
					href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-rounded btn-theme margin-bottom-15 color-white"
				>
					Apply Now
				</a>
			</footer>
		</section>
	)
}

export default GotYouCovered;