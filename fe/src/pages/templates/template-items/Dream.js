import "../../../assets/css/dream.css";

const Dream = ({ cfaData }) => {
	return (
		<section className="dream template-max-width">
			<div className="hero">
				<h1
					contentEditable
					suppressContentEditableWarning={true}
					className="color-blue"
				>
					Your Dream Starts Here
				</h1>
				<a
					contentEditable
					suppressContentEditableWarning={true}
					href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-rounded btn-theme margin-bottom-15 color-white"
				>
					Apply Now
				</a>

				<p
					contentEditable
					suppressContentEditableWarning={true}
					className="color-white"
				>
					Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
				</p>
			</div>

			<div className="content">
				<div className="content-highlight">
					<h3
						contentEditable
						suppressContentEditableWarning={true}
						className="nogaps"
					>
						Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
					</h3>
				</div>

				<div className="content-item">
					<div className="content-item-grid">
						<div className="content-item--left study"></div>
						<div className="content-item--right content-text">
							<div>
								<h2
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig.
								</h2>
								<p
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div className="content-item">
					<div className="content-item-grid">
						<div className="content-item--left content-text">
							<div>
								<h2
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig.
								</h2>
								<p
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
								</p>
							</div>
						</div>
						<div className="content-item--right university"></div>
					</div>
				</div>

				<div className="content-item">
					<div className="content-item-grid">
						<div className="content-item--left grad"></div>
						<div className="content-item--right content-text">
							<div>
								<h2
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig.
								</h2>
								<p
									contentEditable
									suppressContentEditableWarning={true}
									className="nogaps"
								>
									Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
								</p>
							</div>
						</div>
					</div>
				</div>


			</div>

			<footer>
				<div className="content-highlight">
					<h3
						contentEditable
						suppressContentEditableWarning={true}
						className="nogaps margin-bottom-15"
					>
						Spare ribs alcatra boudin corned beef t-bone pig. Brisket meatloaf pork loin prosciutto boudin short ribs. Short loin venison swine spare ribs jerky. Drumstick jerky t-bone, ham ground round flank short loin short ribs pastrami doner. Sausage ham venison doner chislic pancetta. Bacon chicken biltong, turkey doner tongue venison hamburger t-bone pork.
					</h3>

					<a
						contentEditable
						suppressContentEditableWarning={true}
						href={`/applications/forms/${cfaData.form}`} className="btn btn-lg btn-rounded btn-theme margin-bottom-15 color-white"
					>
						Apply Now
					</a>
				</div>
			</footer>
		</section>
	)
}

export default Dream;