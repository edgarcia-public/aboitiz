import "../../assets/css/signuplogin.css";
import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../../App";
import { addUser } from "../../helpers/endpoints/users";
import { checkObjValues } from "../../helpers/checkObjVals";
import SignUpImage from '../../assets/images/utilities/sign-up.jpg';
import { checkSession } from "../../helpers/checkSession";

const SignUp = () => {
  const { setSession, setNotif } = useContext(GlobalContext);
  const session = checkSession();
  const [userData, setUserData] = useState({ first_name: "", middle_name: "", last_name: "", email: "", password: "", retry: "", role: "applicant", terms: "", new_admin: false, status: "pending" });
  const navigate = useNavigate();

  const handleInput = (e) => {
    setUserData({ ...userData, [e.target.name]: e.target.value });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    // check for current session
    if (session.success) return setNotif("You are currently logged in.");

    // validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(userData.email)) return setNotif("Please enter a valid email address.");

    // password requirement > 5 characters
    if (userData.password.length <= 5) return setNotif("The Password should be 6 or more characters.");

    // check confirm password
    if (userData.password.toString() !== userData.retry.toString()) {
      setUserData(userData);
      return setNotif("The Password and Confirm Password didn't match.");
    }

    // check all object values
    if (!checkObjValues(userData)) return setNotif("All fields are required.");

    await addUser(userData)
      .then(response => {
        const sessionData = {
          success: true,
          data: {
            ...response.data,
            due: new Date().getDate()
          }
        }

        setSession(sessionData);
        localStorage.setItem("session", JSON.stringify(sessionData));

        // redirect based on role
        // redirect based on role
        if (sessionData.role === "admin" || sessionData.role === "officer") return navigate("/dashboard");

        return navigate("/dashboard/profile");
      })
      .catch(error => {
        const response = error?.response.data;

        // remove existing session
        localStorage.removeItem("session");
        setNotif(response.data);
      })
  }

  return (
    <section className="SignUp">
      <div className="container">
        <div className="SignUp-flex">
          <div className="SignUp-flex-left">
            <img src={SignUpImage} className="flex-left-image" alt="Aboitiz_Login" />
          </div>

          <div className="SignUp-flex-right">
            <h3>Our aim is to see you succeed. Sign up now!</h3>
            <form onSubmit={handleSubmit}>
              <div className="input-box">
                <input type="text" name="first_name" className="text-capitalize" placeholder="First Name" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <input type="text" name="middle_name" className="text-capitalize" placeholder="Middle Name" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <input type="text" name="last_name" className="text-capitalize" placeholder="Last Name" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <input type="email" name="email" placeholder="Email Address" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <input type="password" name="password" placeholder="Password" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <input type="password" name="retry" placeholder="Confirm Password" onChange={handleInput} required />
              </div>
              <div className="input-box">
                <select id="example-dropdown" name="role" defaultValue="applicant" onChange={handleInput}>
                  <option value="applicant">Applicant</option>
                  <option value="scholar">Scholar</option>
                </select>
              </div>
              <div className="input-box">
                <input type="checkbox" name="terms" onChange={handleInput} required /> Agree to Terms & Use
              </div>
              <button type="submit" className="btn-theme" onClick={handleSubmit}>Sign Up</button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SignUp;
