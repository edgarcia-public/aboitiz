import { useContext } from "react";
import { useParams } from "react-router";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import SubNavbar from "../../layouts/subnavbar/SubNavbar";
import NotFound from "../error/NotFound";
import CreateCFALanding from "../templates/CreateCFALanding";
import UpdateCFALanding from "../../pages/templates/UpdateCFALanding";
import Loading from "../../components/Loading";
import capitalize from "../../helpers/capitalize";

const CFAController = () => {
	const { setNotif } = useContext(GlobalContext);
	const { crud } = useParams();
	const session = checkSession();
	let page = <Loading />;

	// session expired
	if (!session.success) {
		setNotif("Session expired. Please log in.");
		localStorage.removeItem("session");
		window.location.href = "/users/log-in";
	}

	if (session.data.role === "applicant" || session.data.role === "scholar") {
		page = <Loading />;
		window.location.href = "/dashboard/profile";
	}

	switch (crud) {
		case "create":
			page = <CreateCFALanding />
			document.title = `CFA Maker | ${capitalize(session.data.role)}`
			break;
		case "update":
			page = <UpdateCFALanding />
			document.title = `CFA Update | ${capitalize(session.data.role)}`
			break;
		default:
			page = <NotFound />
			break;
	}

	return (
		<>
			<SubNavbar />
			{page}
		</>
	);
};

export default CFAController;
