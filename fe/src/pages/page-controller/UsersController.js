import React, { useContext } from "react";
import { useParams } from "react-router";
import SignUp from "../signup/SignUp";
import NotFound from "../error/NotFound";
import LogIn from "../login/LogIn";
import ResetPassword from "../reset-password/ResetPassword";

const UsersController = () => {
  const { page } = useParams();
  let ui;

  switch (page) {
    case "sign-up":
      ui = <SignUp />;
      document.title = "Sign Up";
      break;
    case "log-in":
      ui = <LogIn />;
      document.title = "Log In";
      break;
    case "reset-password":
      ui = <ResetPassword />;
      document.title = "Reset Password";
      break;
    default:
      ui = <NotFound />;
      break;
  }
  return <section>{ui}</section>;
};

export default UsersController;
