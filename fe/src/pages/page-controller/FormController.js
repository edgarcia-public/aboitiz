import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { addForm, veiwOneForm } from "../../helpers/endpoints/applications";
import SubNavbar from "../../layouts/subnavbar/SubNavbar";
import GenericForm from "../cfa/application-forms/GenericForm";
import BasicForm from "../cfa/application-forms/BasicForm";
import NotFound from "../error/NotFound";
import Loading from "../../components/Loading";

const CFAController = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);
	const { form_title, cfa_id, user_id } = useParams();
	const [formData, setFormData] = useState("");
	let page;

	useEffect(() => {
		if (!formData) {
			const getFormData = async () => {
				await veiwOneForm({ cfa_id: cfa_id, user_id: user_id })
					.then(response => {
						if (!response.data) return setFormData([]);

						setFormData({ ...response.data.form_details, external: response.data.external, _id: response.data._id });
					})
					.catch(error => {
						const response = error.response.data;
						if (response.data === "CFA not found.") {
							setTimeout(() => {
								window.location.href = "/dashboard/profile";
							}, 3000)
							return setNotif("Call For Application Not Found. Redirecting...");
						}
						setNotif(response.data);
					})
			}
			getFormData();
		}
	}, [cfa_id, formData, setNotif, session.data._id, user_id]);

	const handleSaveForm = (userFormData) => {

		// check session first!
		if (!session.success) {
			setNotif("Please log in or sign up.");
			page = <Loading />;

			localStorage.removeItem("session");
			setTimeout(() => {
				window.location.href = "/users/log-in";
			}, 3000)
		}

		const data = {
			...userFormData,
			region: formData.region || userFormData.region,
			province: formData.province || userFormData.province,
			city: formData.city || userFormData.city,
			town: formData.town || userFormData.town,
			user_id: session.data._id,
			cfa_id: cfa_id,
			form_title: form_title
		}

		const saveApplication = async () => {
			await addForm(data)
				.then(response => {
					setFormData({ ...response.data.form_details });
					setNotif("Application form submitted successfully.");
				})
				.catch(error => {
					console.log(error);
				});
		}
		saveApplication();
	}

	if (!formData) return page = <Loading />;

	if (!session.success) return page = <NotFound message={"You will need an active account to view this page. Please sign up or log in."} />;

	switch (form_title) {
		case undefined:
		case "generic":
			document.title = "Generic Application Form | Aboitiz";
			page = <GenericForm formData={formData} setFormData={setFormData} handleSaveForm={handleSaveForm} />
			break;
		case "basic":
			document.title = "Basic Application Form | Aboitiz";
			page = <BasicForm formData={formData} setFormData={setFormData} handleSaveForm={handleSaveForm} />
			break;
		default:
			page = <NotFound />
			break;
	}
	return (
		<>
			<SubNavbar />
			{page}
		</>
	)
};

export default CFAController;
