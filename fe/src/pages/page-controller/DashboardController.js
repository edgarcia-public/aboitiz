import { useContext, useEffect } from "react";
import { useParams } from "react-router";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import Dashboard from "../dashboard/Dashboard";
import Profile from "../profile/Profile";
import Registrations from "../registrations/Registrations";
import ScholarshipsAdmin from "../scholarships/ScholarshipsAdmin";
import NotFound from "../error/NotFound";
import SubNavbar from "../../layouts/subnavbar/SubNavbar";
import ApplicationsAdmin from "../applications/ApplicationsAdmin";
import ApplicationsApplicant from "../applications/ApplicationsApplicant";
import Notifications from "../notifications/Notifications";
import Templates from "../templates/Templates";
import Loading from "../../components/Loading";
import capitalize from "../../helpers/capitalize";
import { viewOneUser } from "../../helpers/endpoints/users";

const DashboardController = () => {
    const { setSession, setNotif } = useContext(GlobalContext);
    const session = checkSession();
    const { page } = useParams();
    let ui;

    // session expired
    if (!session.success) {
        setNotif("Session expired. Please log in.");
        localStorage.removeItem("session");
        window.location.href = "/users/log-in";
    }

    // if new admin or new officer
    if (session.data.new_admin) {
        window.location.href = "/users/reset-password";
    }

    useEffect(() => {
        const user = async () => {
            await viewOneUser(session.data._id)
                .then(response => {

                    const sessionData = {
                        success: true,
                        data: {
                            ...session.data,
                            status: response.data.status,
                            role: response.data.role,
                        }
                    }

                    if (response.data.status !== session.data.status) {
                        localStorage.removeItem("session");

                        setSession(sessionData);
                        localStorage.setItem("session", JSON.stringify(sessionData));
                    }

                    if (response.data.role !== session.data.role) {
                        localStorage.removeItem("session");

                        setSession(sessionData);
                        localStorage.setItem("session", JSON.stringify(sessionData));
                    }
                })
                .catch(error => {
                    setNotif("Something went wrong. Please refresh the page.");
                })
        }
        user();
    }, [session.data, setNotif, setSession])


    switch (page) {
        // the ID is blank like domain.com/dashboard/????
        case "/":
        case "dashboard":
        case undefined:
            ui = <Loading />;

            if (session.data.role === "applicant") window.location.href = "/dashboard/applications";
            if (session.data.role === "scholar") window.location.href = "/dashboard/scholarships";

            ui = <Dashboard />;
            document.title = "Dashboard";
            break;
        case "call-for-application":
            if (session.data.status === "pending" || session.data.role === "applicant" || session.data.role === "scholar") return ui = <div className="container">Unauthorized.</div>

            ui = <Templates />;
            document.title = "New CFA";
            break;
        case "profile":
            ui = <Profile />;
            document.title = "Profile";
            break;
        case "registrations":
            if (session.data.status === "pending" || session.data.role === "applicant" || session.data.role === "scholar") return ui = <div className="container">Unauthorized.</div>

            ui = <Registrations />;
            document.title = `Registrations | ${capitalize(session.data.role)}`;
            break;
        case "scholarships":
            if (session.data.status === "pending" || session.data.role === "applicant") return ui = <div className="container">Unauthorized.</div>

            ui = <ScholarshipsAdmin />;
            document.title = "Scholarships | Aboitiz";
            break;
        case "notifications":
            ui = <Notifications page={page} />;

            document.title = `Notifications  | ${session.data.role === "applicant" ? "Aboitiz" : capitalize(session.data.role)}`;
            break;
        case "applications":
            if (session.data.status === "pending" || session.data.role === "scholar") return ui = <div className="container">Unauthorized.</div>

            document.title = `Applications | ${session.data.role === "applicant" ? "Aboitiz" : capitalize(session.data.role)}`;
            ui = session.data.role === "applicant" ? <ApplicationsApplicant /> : <ApplicationsAdmin />;
            break;
        default:
            ui = <NotFound />;
            break;
    }

    return session ? <><SubNavbar />{ui}</> : undefined;
};

export default DashboardController;
