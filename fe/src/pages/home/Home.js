import { checkSession } from "../../helpers/checkSession";
import SignUp from "../signup/SignUp";

const Home = () => {
  const session = checkSession();
  let page;

  if (!session.success) return <SignUp />;

  document.title = "Home";
  return (
    <section className="Home">
      <div className="container">
        {page}
      </div>
    </section>
  )
};

export default Home;
