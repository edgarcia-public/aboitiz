import "../../assets/css/profile.css";
import { loc } from "../../assets/loc";
import { useContext, useEffect, useState } from "react";
import { viewOneUser } from "../../helpers/endpoints/users";
import { GlobalContext } from "../../App";
import { checkObjValues } from "../../helpers/checkObjVals";
import { updateUser } from "../../helpers/endpoints/users";
import { checkSession } from "../../helpers/checkSession";
import avatarImage from "../../assets/images/utilities/avatar.png";
import AdminForm from "../../pages/profile/AdminForm";
import Loading from "../../components/Loading";
import { NavLink } from "react-router-dom";

const Profile = () => {
  const { setNotif } = useContext(GlobalContext);
  const session = checkSession();
  const [profileData, setProfileData] = useState({
    status: "",
    first_name: "",
    middle_name: "",
    last_name: "",
    email: "",
    age: "",
    gender: "",
    address: "",
    region: "",
    province: "",
    city: "",
    town: "",
  });

  useEffect(() => {
    if (!profileData.status) {
      const getUserProfile = async () => {
        await viewOneUser(session.data._id)
          .then(response => {

            setProfileData({ ...profileData, ...response.data });
          })
          .catch(error => {
            const response = error?.response.data;

            // remove existing session
            localStorage.removeItem("session");
            setNotif(response.data);
          });
      }

      getUserProfile();
    }
  })

  const handleInput = (e) => {
    if (e.target.name === "gender") return setProfileData({ ...profileData, gender: e.target.value });
    if (e.target.name === "region") return setProfileData({ ...profileData, region: e.target.value, province: "", city: "", town: "" });
    if (e.target.name === "province") return setProfileData({ ...profileData, province: e.target.value, city: "", town: "" });
    if (e.target.name === "city") return setProfileData({ ...profileData, city: e.target.value, town: "" });
    if (e.target.name === "town") return setProfileData({ ...profileData, town: e.target.value });

    setProfileData({ ...profileData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!checkObjValues(profileData)) return setNotif("All fields are required.");

    await updateUser(profileData)
      .then(response => {
        setNotif("Profile data has been updated.");
        setProfileData(response.data);
      })
      .catch(error => {
        const response = error?.response.data;
        setNotif(response.data);
      })
  }

  return (
    <section className="Profile">
      <div className="container">
        {profileData.status ?
          <div className="panel">
            <div className="panel-body">
              <div className="Profile-content">
                <div className="Profile-head">
                  <div className="Profile-avatar">
                    <img src={avatarImage} alt="profile" />
                  </div>
                  <p className="nogaps text-uppercase">{profileData.role}</p>
                  <label className="color-gray text-capitalize">{profileData.status}</label>
                </div>
                <div className="Profile-fields">
                  <form onSubmit={handleSubmit}>
                    <div className="grid grid-three gap-15">
                      <div className="input-box">
                        <input type="text" name="first_name" placeholder="First Name" className="text-capitalize" value={profileData.first_name} onChange={handleInput} />
                      </div>
                      <div className="input-box">
                        <input type="text" name="middle_name" placeholder="Middle Name" className="text-capitalize" value={profileData.middle_name} onChange={handleInput} />
                      </div>
                      <div className="input-box">
                        <input type="text" name="last_name" placeholder="Last Name" className="text-capitalize" value={profileData.last_name} onChange={handleInput} />
                      </div>
                    </div>
                    <div className="grid grid-three gap-15">
                      <div className="input-box">
                        <input type="email" disabled value={profileData.email} />
                      </div>
                      <div className="input-box">
                        <input type="number" name="age" placeholder="Age" step="1" min="0" value={profileData.age} onChange={handleInput} />
                      </div>
                      <div className="input-box">
                        <select name="gender" id="profile-gender" value={profileData.gender ? profileData.gender : "label"} onChange={handleInput}>
                          <option value="label" disabled>Gender</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                          <option value="non-binary">Non-binary</option>
                          <option value="none">Prefer not to say</option>
                        </select>
                      </div>
                    </div>
                    <div className="input-box">
                      <input type="text" name="address" placeholder="House no. Street Village, Subdivision" className="text-capitalize" value={profileData.address} onChange={handleInput} />
                    </div>
                    <div className="grid grid-two gap-15">
                      <div className="input-box">
                        <select name="region" value={profileData.region ? profileData.region : "label"} onChange={handleInput}>
                          <option value="label" disabled>Select Region</option>
                          {Object.keys(loc).map((e, i) => <option key={i} value={e}>{loc[e].region_name}</option>)}
                        </select>
                      </div>
                      <div className="input-box">
                        <select name="province" value={profileData.province ? profileData.province : "label"} disabled={profileData.region ? false : true} onChange={handleInput}>
                          <option value="label" disabled>Select Province</option>
                          {profileData.region ? Object.keys(loc[profileData.region].province_list).map((e, i) =>
                            <option key={i} value={e} >{e}</option>)
                            :
                            <option value="label" disabled>Select Province</option>
                          }
                        </select>
                      </div>
                    </div>
                    <div className="grid grid-two gap-15">
                      <div className="input-box">
                        <select name="city" value={profileData.city ? profileData.city : "label"} onChange={handleInput} disabled={profileData.province ? false : true}>
                          <option value="label" disabled>Select City</option>
                          {profileData.province ? Object.keys(loc[profileData.region].province_list[profileData.province].municipality_list).map((e, i) => <option key={i} value={e} >{e}</option>) : <option value="label">Select Municipality</option>}
                        </select>
                      </div>
                      <div className="input-box">
                        <select name="town" value={profileData.town ? profileData.town : "label"} onChange={handleInput} disabled={profileData.city ? false : true}>
                          <option value="label" disabled>Select Town</option>
                          {profileData.city ? loc[profileData.region].province_list[profileData.province].municipality_list[profileData.city].barangay_list.map((e, i) => <option key={i} value={e} >{e}</option>) : <option value="label">Select Municipality</option>}
                        </select>
                      </div>
                    </div>
                    <div className="Profile-action grid grid-two" style={{ justifyItems: "flex-start", alignItems: "center" }}>
                      <button className="btn-theme" onClick={handleSubmit}>UPDATE</button>
                      <NavLink to="/users/reset-password"><small>Reset Password</small></NavLink>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          :
          <Loading />
        }

        {session.data.status === "active" && (session.data.role === "super" || session.data.role === "admin") ? <><hr></hr><AdminForm /></> : undefined}
      </div>
    </section>
  );
};

export default Profile;
