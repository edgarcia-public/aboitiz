import { useContext, useState } from "react";
import { GlobalContext } from "../../App";
import { checkSession } from "../../helpers/checkSession";
import { nanoid } from "nanoid";
import { checkObjValues } from "../../helpers/checkObjVals";
import { addUser } from "../../helpers/endpoints/users";

const AdminForm = () => {
	const session = checkSession();
	const { setNotif } = useContext(GlobalContext);
	const [userData, setUserData] = useState({ first_name: "", middle_name: "", last_name: "", email: "", role: "admin", password: nanoid(10), new_admin: true, status: "pending" });

	const handleInput = (e) => {
		setUserData({ ...userData, [e.target.name]: e.target.value })
	}

	const handleSubmit = async (e) => {
		e.preventDefault();

		// check session before calling API
		if (!session.success) {
			setNotif(session.data);
			localStorage.removeItem("session");

			setTimeout(() => {
				window.location.href = "/";
			}, 3000);
		}

		// check value per field
		if (!checkObjValues(userData)) return setNotif("All fields are required.");

		const addNewAdmin = async () => {
			await addUser(userData)
				.then(response => {
					if (response.success) {
						setUserData({ first_name: "", middle_name: "", last_name: "", email: "", role: "admin", password: nanoid(10) });
						return setNotif(response.data);
					} else {
						return setNotif("Something went wrong. Please refresh the page and try again.");
					}
				})
				.catch(error => {
					const response = error?.response.data;
					setNotif(response.data);
				})
		}

		addNewAdmin();
	}

	return (
		<section className="AdminForm">
			<div className="panel">
				<div className="panel-header bg-gray color-black">Add new Admin or Officer</div>
				<div className="panel-body">
					<form onSubmit={handleSubmit}>
						<div className="grid grid-three gap-15">
							<div className="input-box">
								<label>First Name</label>
								<input type="text" name="first_name" value={userData.first_name || ""} onChange={handleInput} />
							</div>
							<div className="input-box">
								<label>Middle Name</label>
								<input type="text" name="middle_name" value={userData.middle_name || ""} onChange={handleInput} />
							</div>
							<div className="input-box">
								<label>Last Name</label>
								<input type="text" name="last_name" value={userData.last_name || ""} onChange={handleInput} />
							</div>
						</div>
						<div className="grid grid-three gap-15">
							<div className="input-box">
								<label>Email Address</label>
								<input type="email" name="email" value={userData.email || ""} onChange={handleInput} />
							</div>
							<div className="input-box">
								<label>Role</label>
								<select name="role" onChange={handleInput} defaultValue="admin">
									<option value="admin">Admin</option>
									<option value="officer">Business Unit | Officer</option>
								</select>
							</div>
							<div className="input-box">
								<label>Default Password</label>
								<input type="text" name="password" disabled value={userData.password || ""} onChange={handleInput} />
							</div>
						</div>
						<div className="input-box">
							<button type="submit" onClick={handleSubmit}>SUBMIT</button>
						</div>
					</form>
				</div>
				<div className="panel-footer">
					<label className="margin-bottom-5 elem-block color-theme">Important Note:</label>
					<p className="nogaps">Ask the new Admin or Officer user to use the Default Password on first log in. Please save the Default Password before submitting.</p>
				</div>
			</div>
		</section>
	)
}

export default AdminForm;