import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { checkSession } from "../../helpers/checkSession";
import { viewAllCfa } from "../../helpers/endpoints/cfa";
import { GlobalContext } from "../../App";
import formatDatefromUTC from "../../helpers/formatDatefromUTC";
import HTMLTable from "../../components/html-table/HTMLTable";
import Loading from "../../components/Loading";
import capitalize from "../../helpers/capitalize";

const ScholarshipsAdmin = () => {
  const { setNotif } = useContext(GlobalContext);
  const [loading, setLoading] = useState(true);
  const session = checkSession();
  const [tableData, setTableData] = useState("");

  if (session.data.new_admin) {
    window.location.href = "/users/reset-password";
  }

  useEffect(() => {
    if (!tableData) {
      const getCfas = async () => {
        await viewAllCfa()
          .then(response => {
            if (response.data.length) {
              setNotif("Displaying Scholarships data...");

              let data = [];
              response.data.map((obj, index) => data[index] = {
                call_for_applications: obj.title,
                author: obj.author.first_name + " " + obj.author.middle_name + " " + obj.author.last_name,
                status: capitalize(obj.status),
                published: formatDatefromUTC(obj.created_at),
                actions: <>
                  <NavLink to={`/landing/cfa/${obj._id}`} className="text-link" target="_blank" rel="noreferrer">View</NavLink>
                  <span style={{ padding: "0 5px", color: "#ccc" }}>|</span>
                  <NavLink to={`/dashboard/call-for-application/update/${obj._id}`} className="text-link" rel="noreferrer">Edit</NavLink>
                </>
              });

              setTableData(data);
            } else {
              setNotif("No data found.");

              setTableData([]);
            }
            setLoading(false);
          })
          .catch(error => {
            console.log(error);
          })
      }

      getCfas();
    }
  }, [tableData, session.data.role, setNotif]);

  return (
    <section>
      <div className="container">

        <NavLink to={"/dashboard/call-for-application/templates"} className="btn btn-blue margin-bottom-10">New CFA</NavLink>

        {tableData.length ?
          <HTMLTable tableData={tableData} />
          :
          undefined
        }

        {loading ? <Loading /> : undefined}
      </div >
    </section >
  );
};

export default ScholarshipsAdmin;
