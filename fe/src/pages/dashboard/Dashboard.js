import { checkSession } from "../../helpers/checkSession";
import StatsCard from "../../components/StatsCard";
import NotificationItem from "../notifications/NotificationItem";
import Loading from "../../components/Loading";
import { useEffect, useState } from "react";
import { view } from "../../helpers/endpoints/notifications";

const Dashboard = () => {
  const session = checkSession();
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    const getMyNotifications = async () => {
      let q;

      switch (session.data.role) {
        case "super":
          const seeAll = ["super", "admin", "officer", "applicant", "scholar"]

          q = { tos: { $in: seeAll }, active: true }
          break;
        case "admin":
          const admin = ["admin", "officer", "applicant", "scholar"]

          q = { tos: { $in: admin }, active: true }
          break;
        case "officer":
          const officer = ["admin", "officer", "applicant", "scholar"]

          q = { tos: { $in: officer }, active: true }
          break;
        default:
          break;
      }

      await view(q)
        .then(response => {

          setNotifications(response.data);
        })
        .catch(error => {
          console.log(error);
        })
    }

    getMyNotifications();
  }, [session.data._id, session.data.role])

  if (session.data.status !== "active") {
    if (session.data.new_admin) {
      window.location.href = "/users/reset-password";
    } else {
      window.location.href = "/dashboard/profile";
    }
    return <Loading />
  }

  return (
    <section className="Dashbord">
      {session.success && (session.data.role === "super" || session.data.role === "admin" || session.data.role === "officer") ? (
        <>
          <StatsCard />

          <div className="container">
            <hr></hr>
            <h4>Announcements</h4>

            <div className="Notifications">
              {notifications.length ? notifications.map((item, index) => <div className="panel" key={index}><NotificationItem data={item} session={session} page="" /></div>) : undefined}
            </div>
          </div>
        </>
      ) : (
        <Loading />
      )}
    </section>
  );
};
export default Dashboard;
