import page404 from "../../assets/images/utilities/404.png";

// page to be displayed for Page Not Found instances
const NotFound = ({ message }) => {

  document.title = "Page Not Found."
  return (
    <div className="NotFound">
      <div className="container text-center">
        {message ? <h3>{message}</h3> : <h3>Sorry, we can't find the page you're looking for.</h3>}
        <img src={page404} alt="Page Not Found" />
      </div>
    </div>
  )
};

export default NotFound;
