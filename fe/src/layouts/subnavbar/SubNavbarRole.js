const subNavbarRole = {
  admin: [
    { title: "Dashboard", to: "/dashboard" },
    { title: "Applications", to: "/dashboard/applications" },
    { title: "Scholarships", to: "/dashboard/scholarships" },
    { title: "Registrations", to: "/dashboard/registrations" },
    { title: "Notifications", to: "/dashboard/notifications" },
  ],
  super: [
    { title: "Dashboard", to: "/dashboard" },
    { title: "Applications", to: "/dashboard/applications" },
    { title: "Scholarships", to: "/dashboard/scholarships" },
    { title: "Registrations", to: "/dashboard/registrations" },
    { title: "Notifications", to: "/dashboard/notifications" },
  ],
  officer: [
    { title: "Dashboard", to: "/dashboard" },
    { title: "Applications", to: "/dashboard/applications" },
    { title: "Scholarships", to: "/dashboard/scholarships" },
    { title: "Registrations", to: "/dashboard/registrations" },
    { title: "Notifications", to: "/dashboard/notifications" },
  ],
  applicant: [
    { title: "Applications", to: "/dashboard/applications" },
    { title: "Notifications", to: "/dashboard/notifications" },
  ],

  scholar: [
    { title: "Scholarships", to: "/dashboard/scholarships" },
    { title: "Notifications", to: "/dashboard/notifications" },
  ],
  common: [
    { title: "Profile", to: "/dashboard/profile" },
    { title: "", to: "logout" },
  ],
};

export default subNavbarRole;
