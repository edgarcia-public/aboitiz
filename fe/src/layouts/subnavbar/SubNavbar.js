import "../../assets/css/subNavbar.css";
import SubNavbarLink from "./SubNavbarLink";
import linkIcons from "../../assets/icons";
import subNavbarRole from "./SubNavbarRole";
import { useContext } from "react";
import { GlobalContext } from "../../App";
import { NavLink } from "react-router-dom";
import { checkSession } from "../../helpers/checkSession";

const SubNavbar = () => {
  const { setNotif } = useContext(GlobalContext);
  const session = checkSession();

  // admin, officer, applicant, scholar
  let dynamicLinks = subNavbarRole[`${session.data.role}`];

  const handleLogOut = () => {
    // remove browser session
    localStorage.removeItem("session");
    setNotif("Log out successfully");
    window.location.href = "/";
  };

  return (
    <section className="SubNavbar">
      <div className="container">
        <div className="SubNavbar-dynamic">
          <div className="SubNavbar-dynamic-left">
            {session.data.status === "active"
              ? dynamicLinks.map((e, index) => (
                <SubNavbarLink
                  key={index}
                  icon={
                    linkIcons[
                    e.title.split(" ").join().toLocaleLowerCase()
                    ] || linkIcons["none"]
                  }
                  title={e.title}
                  to={e.to}
                />
              ))
              : undefined}
          </div>
          <div className="SubNavbar-dynamic-right">
            <nav className="SubNavbarLink">
              <NavLink
                className="SubNavbarLink-content"
                to={"/dashboard/profile"}
              >
                <div className="SubNavbarLink-icon">{linkIcons["profile"]}</div>
                <div className="SubNavbarLink-title">Profile</div>
              </NavLink>
            </nav>
            <nav
              className="SubNavbarLink"
              onClick={handleLogOut}
              style={{ borderLeft: "1px solid #e4e4e4" }}
            >
              <NavLink
                className="SubNavbarLink-content"
                to={"/"}
              >
                <div className="SubNavbarLink-icon">{linkIcons["none"]}</div>
              </NavLink>
            </nav>
          </div>
        </div>
      </div>
    </section>
  );
};

export default SubNavbar;
