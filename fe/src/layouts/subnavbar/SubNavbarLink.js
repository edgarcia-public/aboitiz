import { NavLink } from "react-router-dom";
import "../../assets/css/subNavbar.css";

const SubNavbarLink = ({ icon, title, to }) => {

  return (
    <nav className="SubNavbarLink">
      <NavLink className="SubNavbarLink-content" end to={to}>
        <div className="SubNavbarLink-icon">{icon}</div>
        <div className="SubNavbarLink-title">{title}</div>
      </NavLink>
    </nav>
  );
};

export default SubNavbarLink;
