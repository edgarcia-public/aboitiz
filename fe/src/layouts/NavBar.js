import "../assets/css/navbar.css";
import { NavLink, useParams } from 'react-router-dom';
import { checkSession } from "../helpers/checkSession";

const Navbar = () => {
  const session = checkSession();
  const { page } = useParams();

  return (
    <nav className="NavBar">
      <div className="container">
        <div className="NavBar-grid">
          <div className="NavBar-grid-left">
            <div className="grid-left-logo">
              <img src={require("../assets/images/utilities/aboitiz-logo.png")} className="logo" alt="Aboitiz-Logo" />
            </div>
            <form className="grid-left-search">
              <input type="text" placeholder="Search..." />
              <button type="submit" className="btn-theme">GO</button>
            </form>
          </div>
          <div className="NavBar-grid-right">
            <div className="grid-right-menu">
              <NavLink end to="/" className="right-menu-link">Home</NavLink>
              {session.success ?
                <NavLink to={(session.data.role === "super" || session.data.role === "admin" || session.data.role === "officer") && session.data.status === "active" ? "/dashboard" : "/dashboard/profile"} className={`right-menu-link ${page ? "active" : undefined}`}>My Dash</NavLink>
                :
                <>
                  <NavLink end to="/users/sign-up" className="right-menu-link">Sign Up</NavLink>
                  <NavLink end to="/users/log-in" className="right-menu-link">Log In</NavLink>
                </>
              }
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
