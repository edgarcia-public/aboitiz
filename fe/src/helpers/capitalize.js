const capitalize = (words) => {
  if (typeof words !== 'string' || words.length === 0) {
    return words;
  }

  const a = words.toLowerCase();

  if (a.trim().indexOf(' ') === -1) {
    return a.charAt(0).toUpperCase() + a.slice(1);
  }

  return a.replace(/\b\w/g, (match) => match.toUpperCase());
};

export default capitalize;
