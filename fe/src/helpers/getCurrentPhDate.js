const getCurrentPhDate = () => {
  const date = new Date().toLocaleString("en-US", { timeZone: "Asia/Manila" });
  const options = {
    weekday: "short",
    month: "short",
    day: "numeric",
    year: "numeric",
  };
  const formattedDate = new Date(date).toLocaleDateString("en-US", options);
  return formattedDate;
};

export default getCurrentPhDate;
