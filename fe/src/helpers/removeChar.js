const removeChar = (str, toRemove) => {
  const reg = new RegExp("[" + toRemove + "]", "g");
  return str.replace(reg, " ");
};

export default removeChar;
