import axios from "axios";

const NOTIFICATIONS_API = "http://localhost:4000/notifications";

export const add = async (notifData) => {
	const { data } = await axios.post(`${NOTIFICATIONS_API}/add`, notifData);
	return data;
};

export const patch = async (notifData) => {
	const { data } = await axios.patch(`${NOTIFICATIONS_API}/update`, notifData);
	return data;
};

export const view = async (notifData) => {
	const { data } = await axios.get(`${NOTIFICATIONS_API}/view`, {
		params: notifData
	});
	return data;
};

export const viewOne = async (notifId) => {
	const { data } = await axios.get(`${NOTIFICATIONS_API}/viewOne`, {
		params: notifId
	});
	return data;
};

