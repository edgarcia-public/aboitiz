import axios from "axios";

const APPLICATIONS_API = "http://localhost:4000/applications";

export const addForm = async (applicationData) => {
  const { data } = await axios.post(`${APPLICATIONS_API}/add`, applicationData);
  return data;
};

export const veiwOneForm = async (query) => {
  const { data } = await axios.get(`${APPLICATIONS_API}/view`, {
    params: query
  })
  return data;
};

export const viewAllForms = async (query) => {
  const { data } = await axios.get(`${APPLICATIONS_API}/view-all`, {
    params: query
  });
  return data;
};

export const viewUserForm = async (query) => {
  const { data } = await axios.get(`${APPLICATIONS_API}/view-user-form`, {
    params: query
  });
  return data;
};

export const updateStatus = async (formData, query) => {
  const { data } = await axios.patch(`${APPLICATIONS_API}/update-status`, formData, {
    params: query
  });
  return data;
};

export const updateFormDetails = async (formDetailsData) => {
  const { data } = await axios.patch(`${APPLICATIONS_API}/update-form-details`, formDetailsData);
  return data;
};