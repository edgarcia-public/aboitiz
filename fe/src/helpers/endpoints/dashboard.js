import axios from "axios";

const DASHBOARD_API = "http://localhost:4000/dashboard";

export const viewAllDashData = async () => {
	const { data } = await axios.get(`${DASHBOARD_API}/view-all-dash-data`);
	return data;
};