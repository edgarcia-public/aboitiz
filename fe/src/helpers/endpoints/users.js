import axios from "axios";

const USERS_API = "http://localhost:4000/users";

export const addUser = async (userData) => {
  const { data } = await axios.post(`${USERS_API}/add`, userData);
  return data;
};

export const logInUser = async (userData) => {
  const { data } = await axios.post(`${USERS_API}/log-in`, userData);
  return data;
};

export const viewOneUser = async (userId) => {
  const { data } = await axios.get(`${USERS_API}/view/${userId}`);
  return data;
};

export const updateUser = async (userData) => {
  const { data } = await axios.patch(`${USERS_API}/update`, userData);
  return data;
};

export const updateStatus = async (userData, query) => {
  const { data } = await axios.patch(`${USERS_API}/update-status`, userData, {
    params: query
  });
  return data;
};

export const viewStatus = async (status) => {
  const { data } = await axios.get(`${USERS_API}/view`, {
    params: status
  });
  return data;
};

export const resetPassword = async (userData) => {
  const { data } = await axios.patch(`${USERS_API}/reset-password`, userData);
  return data;
};