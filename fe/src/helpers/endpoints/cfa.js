import axios from "axios";

const CFA_API = "http://localhost:4000/cfa";

export const addCfa = async (cfaData) => {
  const { data } = await axios.post(`${CFA_API}/add`, cfaData);
  return data;
};

export const updateCfa = async (cfaData) => {
  const { data } = await axios.put(`${CFA_API}/update`, cfaData);
  return data;
};

export const viewOneCfa = async (cfaId) => {
  const { data } = await axios.get(`${CFA_API}/view/${cfaId}`);
  return data;
};

export const viewAllCfa = async () => {
  const { data } = await axios.get(`${CFA_API}/view`);
  return data;
};
