// checks user's session before calling APIs
export const checkSession = () => {
	const dateNow = new Date().getDate();
	const session = JSON.parse(localStorage.getItem("session"));

	// NO session
	if (!session) return { success: false, data: "Session expired. Please log in." }

	// check expiration
	if (Number(dateNow) !== Number(session.data.due)) return { success: false, data: "Session expired. Please log in." }

	// ACTIVE session
	return { success: true, ...session }
}