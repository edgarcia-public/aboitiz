export const checkObjValues = (obj) => {

	if (obj === null || obj === undefined) return false;

	for (let key in obj) {
		if (obj[key] === undefined || obj[key] === null || obj[key] === "") {
			return false;
		}
	}
	return true;
}