const formatDatefromUTC = (utcDate) => {
  const options = { month: "short", day: "numeric", year: "numeric" };
  const date = new Date(utcDate);
  const formattedDate = date.toLocaleDateString("en-PH", options);
  return formattedDate;
};

export default formatDatefromUTC;
