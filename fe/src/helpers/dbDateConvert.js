const dbDateConvert = (str) => {
	const date = new Date(str);

	const options = { year: 'numeric', month: 'long', day: 'numeric' };
	const nameDate = date.toLocaleDateString('en-US', options);

	return nameDate;
};

export default dbDateConvert;
