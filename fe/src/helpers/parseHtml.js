export const parseHtml = (data) => {
	const parser = new DOMParser();
	const doc = parser.parseFromString(data.canvas, "text/html").documentElement.outerHTML;

	return (
		<div id="Templates-one-parent">
			<div dangerouslySetInnerHTML={{ __html: doc }} />
		</div>
	);
}