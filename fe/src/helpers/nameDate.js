const nameDate = (numberedDate) => {
  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  // return ONLY numbers
  const num = numberedDate.replace(/\D/g, "");

  const year = num.slice(-4);
  const month = parseInt(num.slice(0, 2), 10);
  const day = parseInt(num.slice(2, 4), 10);

  return `${monthNames[month - 1]} ${day}, ${year}}`;
};

export default nameDate;

